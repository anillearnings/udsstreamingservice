package com.fedex.uds.streaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableJpaRepositories
@EntityScan(basePackages = {"com.fedex.uds.view.phuwvevententity.entity",
        "com.fedex.uds.view.interceptpublishentity.entity", "com.fedex.uds.view.shipmentpublishentity.entity", "com.fedex.uds.view.sortmodifierentity.entity"})
@ComponentScan(basePackages = "com.fedex.uds")
@EnableFeignClients
public class UdsStreamingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UdsStreamingServiceApplication.class, args);

    }

}
