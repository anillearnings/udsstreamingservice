package com.fedex.uds.streaming.repository;

import com.fedex.uds.view.sortmodifierentity.entity.SortModifierServiceEntity;
import com.fedex.uds.view.sortmodifierentity.entity.SortModifierServiceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SortModifierRepository extends JpaRepository<SortModifierServiceEntity, SortModifierServiceId> {

    public SortModifierServiceEntity findFirstBySortModifierServiceId_handlingUnitUUIDOrderByEventCreateTimestampDesc(String handlingUnitUUID);

}
