package com.fedex.uds.streaming.repository;

import com.fedex.uds.view.phuwvevententity.entity.HandlingUnitId;
import com.fedex.uds.view.phuwvevententity.entity.PhuwvEventsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhuwvEventRepository extends JpaRepository<PhuwvEventsEntity, HandlingUnitId> {

    public List<PhuwvEventsEntity> findByHandlingUnitId_handlingUnitUUID(String handlingUnitUUID);

    @Query(value = "select * from phuwv_events where handling_unit_uuid_id=?1 AND event_uuid_id=?2 ", nativeQuery = true)
    public PhuwvEventsEntity findByHandlingUnitId_handlingUnitUUIDeventUUID(String handlingUnitUUID, String eventUUID);

    public List<PhuwvEventsEntity> findByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
            String handlingUnitUUID);

    public PhuwvEventsEntity findTopByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(
            String handlingUnitUUID);

    public PhuwvEventsEntity findFirstByEnterpriseHandlingUnitIdOrderByEventCreateTimestampDesc(String trknbr);
}
