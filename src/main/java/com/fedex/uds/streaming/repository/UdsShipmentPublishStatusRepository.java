package com.fedex.uds.streaming.repository;

import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UdsShipmentPublishStatusRepository
        extends JpaRepository<UdsShipmentPublishStatusEntity, UdsShipmentPublishStatusId> {

    public List<UdsShipmentPublishStatusEntity> findByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);

    public UdsShipmentPublishStatusEntity findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);


    @Query(value = "select * from UDS_ADAPTOR_SCHEMA.uds_shipment_publish_status where enterprise_handling_unit_id=? order by row_update_tmstp", nativeQuery = true)
    public List<UdsShipmentPublishStatusEntity> findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(
            String trackingNumber);


    public UdsShipmentPublishStatusEntity findTopByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);


}
