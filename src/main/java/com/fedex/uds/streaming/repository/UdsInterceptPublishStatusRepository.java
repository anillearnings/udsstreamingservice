package com.fedex.uds.streaming.repository;

import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface UdsInterceptPublishStatusRepository
        extends JpaRepository<UdsInterceptPublishStatusEntity, UdsInterceptPublishStatusId> {

    public List<UdsInterceptPublishStatusEntity> findByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);


    public UdsInterceptPublishStatusEntity findFirstByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);

    public UdsInterceptPublishStatusEntity findTopByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
            String handlingUnitUUID);


    @Query(value = "select * from UDS_ADAPTOR_SCHEMA.uds_intercept_publish_status where handling_unit_uuid_id= :handlingUnitUUID AND phuwv_event_uuid_id= :phuwvEventUUID order by row_update_tmstp desc ", nativeQuery = true)
    public List<UdsInterceptPublishStatusEntity> findByHandlingUnitUUIDAndPhuwvEventUUID(@Param("handlingUnitUUID") String handlingUnitUUID, @Param("phuwvEventUUID") String phuwvEventUUID);

    @Query(value = "select * from UDS_ADAPTOR_SCHEMA.uds_intercept_publish_status  where handling_unit_uuid_id= :handlingUnitUUID AND UDS_INTERCEPT_CD IN :udsInterceptCode AND PRIMARY_INTERCEPT_LOC_CD IN :primaryInterceptLocationCode order by row_update_tmstp desc ", nativeQuery = true)
    public List<UdsInterceptPublishStatusEntity> findByHandlingUnitUUIDAndUdsInterceptCodeAndPrimaryInterceptLocationCode(@Param("handlingUnitUUID") String handlingUnitUUID, @Param("udsInterceptCode") Collection<String> udsInterceptCode, @Param("primaryInterceptLocationCode") Collection<String> primaryInterceptLocationCode);

    @Query(value = "select * from UDS_ADAPTOR_SCHEMA.uds_intercept_publish_status where enterprise_handling_unit_id=? order by row_update_tmstp", nativeQuery = true)
    public List<UdsInterceptPublishStatusEntity> findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(
            String trackingNumber);

}


