package com.fedex.uds.streaming.client;

import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@FeignClient(name = "uds-translation-mapping-service", url = "${app.rest-client.end-points.TRANSLATION-MAPPING-SERVICE.base-uri}")
public interface TranslationMappingServiceClient {

    @PostMapping(value = "/api/v1/uds/mapping/dgCodes", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<UdsDGMappingEntity>> getDgCodes(@RequestBody List<UdsDGMappingEntity> udsDGMappingEntities);

    @PostMapping(value = "/api/v1/uds/mapping/serviceCodes", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<UdsServiceCodeMappingEntity>> getServiceCodes(@RequestBody List<UdsServiceCodeMappingEntity> udsServiceCodeMappingEntities);

    @PostMapping(value = "/api/v1/uds/mapping/handlingCodes", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<UdsHandlingCodeMappingEntity>> getHandlingCodes(@RequestBody List<UdsHandlingCodeMappingEntity> udsHandlingCodeMappingEntities);

    @PostMapping(value = "/api/v1/uds/mapping/translations", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<MappingCodesDTO> getTranslations(@RequestBody MappingCodesDTO mappingCodeDTO);

    @GetMapping(value = "/api/v1/uds/mapping/allMappings", produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<MappingCodesDTO> getAllMappingCodes();
}
