package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fedex.uds.common.dto.UdsShipmentPublishStatusDTO;
import com.fedex.uds.streaming.repository.UdsShipmentPublishStatusRepository;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class UdsShipmentPublishStatusService {

    @Autowired
    UdsShipmentPublishStatusRepository udsShipmentPublishStatusRepository;

    public UdsShipmentPublishStatusDTO getLatestUdsShipmentPublishStatusByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        UdsShipmentPublishStatusDTO dto = new UdsShipmentPublishStatusDTO();

        UdsShipmentPublishStatusEntity shipmentpublishstatusentity = udsShipmentPublishStatusRepository
                .findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);

        if (shipmentpublishstatusentity != null) {

            dto.setPhuwvEventUUID(shipmentpublishstatusentity.getPhuwvEventUUID());
        }
        return dto;

    }

    public String getLatestUdsShipmentPublishStatusXmlByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        String xml = null;
        UdsShipmentPublishStatusEntity shipmentpublishstatusentity = udsShipmentPublishStatusRepository
                .findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);
        if (shipmentpublishstatusentity != null)
            xml = getXmlFromDataImg(shipmentpublishstatusentity);
        return xml;

    }

    public String getLatestUdsShipmentPublishStatusXmlTopByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        String xml = null;
        UdsShipmentPublishStatusEntity shipmentpublishstatusentity = udsShipmentPublishStatusRepository
                .findTopByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);
        if (shipmentpublishstatusentity != null)
            xml = getXmlFromDataImg(shipmentpublishstatusentity);
        return xml;

    }

    public String getLatestUdsShipmentPublishStatusXmlByEnterpriseHandlingUnitId(String trackingNumber) throws Exception {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusXmlByEnterpriseHandlingUnitId trackingNumber: "
                + trackingNumber);

        String xml = null;
        List<UdsShipmentPublishStatusEntity> shipmentpublishstatusentity = udsShipmentPublishStatusRepository
                .findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(trackingNumber);
        if (shipmentpublishstatusentity != null)
            xml = getXmlFromDataImg(shipmentpublishstatusentity.get(0));
        return xml;
    }

    public String getXmlFromDataImg(UdsShipmentPublishStatusEntity entity) throws IOException {
        // Update as data columns are added
        byte[] dataImage1 = entity.getShipmentDataImg1();
        byte[] dataImage2 = entity.getShipmentDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }

        return null;
    }

}
