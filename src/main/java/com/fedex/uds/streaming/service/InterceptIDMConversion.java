package com.fedex.uds.streaming.service;

import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.common.dto.idm.*;
import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class InterceptIDMConversion {
    @Autowired
    public HandlingUnitHandler handler;

    @Autowired
    @Qualifier("TranslationMappingServiceInvoker")
    private TranslationMappingServiceInvoker mappingServiceInvoker;

    @Autowired
    private FormIdCheckProperties formIdCheck;

    public List<MainShipment> performInterceptIdmConversion(HandlingUnit handlingUnit) {

        List<MainShipment> listofshipments = new ArrayList<MainShipment>();
        if (handlingUnit != null) {
            if (handlingUnit.getWorkRequirements() != null) {


                Map<String, String> translatedHandlingCodes = new HashMap<String, String>();
                List<String> reqhandlingcodes = new ArrayList<String>();

                MappingCodesDTO mappingDTORequest = new MappingCodesDTO();
                List<UdsDGMappingEntity> dgMappingEntityListRequest = new ArrayList<>();
                UdsDGMappingEntity entityRequest = new UdsDGMappingEntity();
                List<UdsServiceCodeMappingEntity> serviceCodeMappingEntityListRequest = new ArrayList<>();
                UdsServiceCodeMappingEntity serviceEntityRequest = new UdsServiceCodeMappingEntity();
                List<UdsHandlingCodeMappingEntity> handlingCodeMappingEntityListRequest = new ArrayList<>();
                serviceEntityRequest.setServiceCd("RM");// (handlingUnit.getLegacyServiceCode()!=null?
                // handlingUnit.getLegacyServiceCode():null);
                serviceEntityRequest.setCommitTm("2359");
                entityRequest.setOperatingCompanyCd("TNT");// (handlingUnit.getShipment().getOwningOpCo());
                if (handlingUnit.getSpecialHandlingCodes() != null && !handlingUnit.getSpecialHandlingCodes().isEmpty()) {
                    entityRequest.setSourceDgTypeCd(handlingUnit.getSpecialHandlingCodes().get(0));
                }
                dgMappingEntityListRequest.add(entityRequest);
                serviceCodeMappingEntityListRequest.add(serviceEntityRequest);


                for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {

                    String procedurecode = null;
                    for (int i = 0; i < requirement.getAttributes().size(); i++) {
                        if (requirement.getAttributes().get(i).getKey().equals(Constants.PROCEDURE_CODES)) {
                            procedurecode = requirement.getAttributes().get(i).getValues().get(0);
                        }
                    }

                    if (procedurecode != null) {

                        UdsHandlingCodeMappingEntity udshandlingentityRequest = new UdsHandlingCodeMappingEntity();
                        udshandlingentityRequest.setHandlingInterceptCd(procedurecode);
                        handlingCodeMappingEntityListRequest.add(udshandlingentityRequest);
                        reqhandlingcodes.add(procedurecode);
                    } else {

                        log.info("No Procedure code found : hence not calling translation ");

                    }

                }

                mappingDTORequest.setUdsHandlingCodeMappingEntities(handlingCodeMappingEntityListRequest);
                mappingDTORequest.setUdsServiceCodeMappingEntities(serviceCodeMappingEntityListRequest);
                mappingDTORequest.setUdsDGMappingEntities(dgMappingEntityListRequest);

                MappingCodesDTO translationsReqResult = mappingServiceInvoker
                        .getTranslationsResult(mappingDTORequest);

                log.info("Size of Response:  " + translationsReqResult.getUdsHandlingCodeMappingEntities().size());
                log.info("Size of Request:  " + reqhandlingcodes.size());


                if (translationsReqResult.getUdsHandlingCodeMappingEntities().size() == reqhandlingcodes.size()) {
                    for (int i = 0; i < reqhandlingcodes.size(); i++) {
                        translatedHandlingCodes.put(reqhandlingcodes.get(i), translationsReqResult.getUdsHandlingCodeMappingEntities().get(i).getSorterInterceptCd());
                    }
                }


                for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
                    //handlingUnit.getWorkRequirements().forEach(requirement -> {

                    MainShipment shipment = new MainShipment();
                    SHP170 shipmentDetail = new SHP170();
                    SHP010 shipmentHeader = new SHP010();
                    SHP160 shipmentHandlingCode = new SHP160();

                    ArrayList<SHP160> shipmentHandlingCodes = new ArrayList<SHP160>();

                    shipmentHeader.setCom010CdOrig(" ");
                    shipmentHeader.setCom010CdDest(" ");
                    shipmentHeader.setBul010IdOrig(" ");
                    shipmentHeader.setBul010IdDest(" ");
                    shipmentHeader.setBul010IdCollection(" ");

                    log.info("IDM Intercept Type: " + requirement.getType());
                    if (requirement.getType() != null) {
                        if (requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)
                                || requirement.getType().equalsIgnoreCase(Constants.GEOGRAPHIC_INTERCEPT)
                                || requirement.getType().equalsIgnoreCase(Constants.FACILITY_INTERCEPT)) {

//clearance, trade-restriction
                            // mapped
                            for (HandlingUnitId h : handlingUnit.getHandlingUnitIds()) {
                                if (h.getIdType().equals(Constants.ENTERPRISE) || h.getIdType().equals(Constants.FEDEX)
                                        || h.getIdType().equals(Constants.EXPRESS)) {
                                    shipmentHeader.setCustomerReferenceNr(h.getId());
                                }
                                if (h.getIdType().equals(Constants.FRANCE_DOMESTIC)) {
                                    shipmentDetail.setDomesticShipmentNr(h.getId());
                                    shipmentDetail.setDomesticShipmentType(h.getIdType());
                                    shipmentHeader.setId(h.getId());
                                }
                                if (h.getIdType().equals(Constants.TNT)) {
                                    shipmentHeader.setId(h.getId());
                                }

                            }

                            if (handlingUnit.getShipment().getCustomerAssociations() != null && handlingUnit
                                    .getShipment().getCustomerAssociations().get(0).getAlternateAccount() != null) {

                                shipmentHeader.setLegacyConNr(handlingUnit.getShipment().getCustomerAssociations()
                                        .get(0).getAlternateAccount().getId());
                            }
                            // handlingUnit.getId
                            // shipmentdetails.setCustom
                            // shipmentHeader.setLegacyConNr(shipmentDetail.ge)


                            String clnStnClsCd = requirement.getAdditionalAttributes().stream()
                                    .filter(handling -> handling.getKey().equals("clnStnClsCd")).findAny().get()
                                    .getValues().get(0);

                            shipmentHandlingCode.setId(shipmentHeader.getCustomerReferenceNr() + "_" + clnStnClsCd);


                            String procedurecode = null;
                            for (int i = 0; i < requirement.getAttributes().size(); i++) {
                                if (requirement.getAttributes().get(i).getKey().equals(Constants.PROCEDURE_CODES)) {
                                    procedurecode = requirement.getAttributes().get(i).getValues().get(0);
                                }
                            }

                            if (procedurecode == null) {
                                log.info("No Procedure code found : hence not publishing : returning null ");
                                return null;
                            }

                            /*
                             * if (handlingUnit.getHandlingUnitIds() != null) {
                             * shipmentHeader.setCustomerReferenceNr(handlingUnit.getHandlingUnitIds().
                             * stream() .filter(handling ->
                             * handling.getIdType().equals("ENTERPRISE")).findAny().get().getId()); }
                             */

                            /*
                             * if (handlingUnit.getHandlingUnitIds() != null) {
                             * shipmentDetail.setDomesticShipmentNr(handlingUnit.getHandlingUnitIds().stream
                             * () .filter(handling ->
                             * handling.getIdType().equals("FRANCE_DOMESTIC")).findAny().get() .getId()); }
                             *
                             * if (handlingUnit.getHandlingUnitIds() != null) {
                             * shipmentDetail.setDomesticShipmentType(handlingUnit.getHandlingUnitIds().
                             * stream() .filter(handling ->
                             * handling.getIdType().equals("FRANCE_DOMESTIC")).findAny().get() .getId()); }
                             */
                            // shipmentHandlingCode.setHac010Id("CUI");// not able to find

                            // additionalAttributes node

                            // shipmentHeader.setLegacyConNr("");
//                                                            handlingUnit.getShipment().getCustomerAssociations().forEach(customer -> {
//                                                                            customer.get
//                                                            });

                            List<String> orangeIds = formIdCheck.getFormId();
                            Boolean orangePackage = orangeIds.contains(handlingUnit.getFormID());
                            if (orangePackage) {
                                shipmentDetail.setSourceSystemCd("TNT");
                            } else {
                                shipmentDetail.setSourceSystemCd("FX");
                                if (handlingUnit.getShipment() != null) {
                                    shipmentDetail
                                            .setMasterShipmentId(handlingUnit.getShipment().getMasterTrackingNumber());
                                }
                            }

                            if (handlingUnit.getWaypoints() != null && !(handlingUnit.getWaypoints().isEmpty())) {
                                handlingUnit.getWaypoints().forEach(waypoint -> {
                                    if (waypoint.getType() != null) {
                                        if (waypoint.getType().equalsIgnoreCase(Constants.DESTINATION)) {
                                            if (requirement.getAttributes() != null && !requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)) {
                                                List<String> values = requirement.getAttributes().stream()
                                                        .filter(attributes -> attributes.getKey()
                                                                .equalsIgnoreCase(Constants.LOCATION_CODES))
                                                        .findAny().get().getValues();
                                                shipmentHandlingCode
                                                        .setBul010IdOccuring(values != null ? values.get(0) : null);
                                            }else {
                                                shipmentHandlingCode.setBul010IdOccuring(waypoint.getFacilityCode() != null ? waypoint.getFacilityCode() : " ");
                                            }
                                            shipmentHeader.setBul010IdDest(waypoint.getFacilityCode() != null ? waypoint.getFacilityCode() : " ");
                                        }
                                        if (waypoint.getType().equalsIgnoreCase(Constants.ORIGIN) && waypoint.getFacilityCode() != null) {
                                            shipmentHeader.setBul010IdCollection(waypoint.getFacilityCode());
                                            shipmentHeader.setBul010IdOrig(waypoint.getFacilityCode());
                                        }
                                    }
                                });

                            }

                            MappingCodesDTO mappingDTO = new MappingCodesDTO();
                            List<UdsDGMappingEntity> dgMappingEntityList = new ArrayList<>();
                            UdsDGMappingEntity entity = new UdsDGMappingEntity();
                            List<UdsServiceCodeMappingEntity> serviceCodeMappingEntityList = new ArrayList<>();
                            UdsServiceCodeMappingEntity serviceEntity = new UdsServiceCodeMappingEntity();
                            List<UdsHandlingCodeMappingEntity> handlingCodeMappingEntityList = new ArrayList<>();
                            UdsHandlingCodeMappingEntity udshandlingentity = new UdsHandlingCodeMappingEntity();

                            udshandlingentity.setHandlingInterceptCd(procedurecode);

                            serviceEntity.setServiceCd("RM");// (handlingUnit.getLegacyServiceCode()!=null?
                            // handlingUnit.getLegacyServiceCode():null);
                            serviceEntity.setCommitTm("2359");
                            entity.setOperatingCompanyCd("TNT");// (handlingUnit.getShipment().getOwningOpCo());
                            entity.setSourceDgTypeCd(handlingUnit.getSpecialHandlingCodes().get(0).toString());
                            dgMappingEntityList.add(entity);
                            handlingCodeMappingEntityList.add(udshandlingentity);
                            serviceCodeMappingEntityList.add(serviceEntity);
                            mappingDTO.setUdsHandlingCodeMappingEntities(handlingCodeMappingEntityList);
                            mappingDTO.setUdsServiceCodeMappingEntities(serviceCodeMappingEntityList);
                            mappingDTO.setUdsDGMappingEntities(dgMappingEntityList);

                            if (translationsReqResult.getUdsHandlingCodeMappingEntities().size() != reqhandlingcodes.size()) {
                                MappingCodesDTO translationsResult = mappingServiceInvoker
                                        .getTranslationsResult(mappingDTO);


                                if (translationsResult.getUdsHandlingCodeMappingEntities() != null
                                        && translationsResult.getUdsHandlingCodeMappingEntities().get(0) != null
                                        && translationsResult.getUdsHandlingCodeMappingEntities().get(0)
                                        .getSorterInterceptCd() != null) {
                                    shipmentHandlingCode.setHac010Id(translationsResult.getUdsHandlingCodeMappingEntities()
                                            .get(0).getSorterInterceptCd());
                                }


                            } else {
                                shipmentHandlingCode.setHac010Id(translatedHandlingCodes.get(procedurecode));
                            }

                            requirement.getStatuses().forEach(statuses -> { // has to be externalized
                                if (statuses.getValue().equalsIgnoreCase(Constants.ACTIVE)) {
                                    shipmentHandlingCode.setHandlingCodeReleaseInd("N");
                                }
                                if (statuses.getValue().equalsIgnoreCase(Constants.RELEASE)) {
                                    shipmentHandlingCode.setHandlingCodeReleaseInd("Y");
                                }
                            });
                            /*
                             * shipmentHeader.setBul010IdCollection(handlingUnit.getWaypoints().stream()
                             * .filter(waypoint -> { if(waypoint.getType()!=null) { return
                             * waypoint.getType().equals("ORIGIN").findAny().get().getFacilityCode(); }
                             *
                             */
                            if (requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT))
                                shipmentHandlingCode.setGlobalCd("Y");
                            else
                                shipmentHandlingCode.setGlobalCd("N");

                            // setting shipmenthandlingcode to mainshipment
                            if (shipmentHandlingCode.getHac010Id() != null
                                    && !shipmentHandlingCode.getHac010Id().isEmpty()) {
                                shipmentHandlingCodes.add(shipmentHandlingCode);
                                shipment.setShipmentHandlingCode(shipmentHandlingCodes);
                            }

                            /*
                             * shipmentHeader.setCom010CdOrig(handlingUnit.getOriginCountryCode());
                             * shipmentHeader.setCom010CdDest(handlingUnit.getDestinationCountryCode());
                             */
                            if (handlingUnit.getShipment() != null) {
                                if (handlingUnit.getShipment().getCustomerAssociations() != null
                                        && !(handlingUnit.getShipment().getCustomerAssociations().isEmpty())) {
                                    handlingUnit.getShipment().getCustomerAssociations().forEach(associations -> {
                                        if (associations.getCustomerType() != null) {
                                            if (associations.getCustomerType().equals(Constants.RECIPIENT)) {
                                                shipmentHeader
                                                        .setCom010CdDest(handlingUnit.getDestinationCountryCode() != null ? handlingUnit.getDestinationCountryCode() : " ");

                                            } else if (associations.getCustomerType().equals(Constants.SHIPPER)) {
                                                shipmentHeader.setCom010CdOrig(handlingUnit.getOriginCountryCode() != null ? handlingUnit.getOriginCountryCode() : " ");
                                            }
                                        }
                                    });
                                }
                            }

                            //			shipmentHeader.setBul010IdDest("");
                            ArrayList<SHP880> shipmentRouteList = new ArrayList<SHP880>();
                            if (requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)) {
                                for (int i = 0; i < handlingUnit.getWaypoints().size(); i++) {

                                    SHP880 shipmentRoute = new SHP880();

                                    Waypoint waypoint = handlingUnit.getWaypoints().get(i);
                                    if ((i + 1) == handlingUnit.getWaypoints().size()) {
                                        break;
                                    } else {
                                        if (waypoint.getSequenceNumber() != null) {
                                            shipmentRoute.setSequencingNr(waypoint.getSequenceNumber().toString());
                                            if (shipmentHeader.getCustomerReferenceNr() != null) {
                                                shipmentRoute.setId(shipmentHeader.getCustomerReferenceNr() + "_"
                                                        + clnStnClsCd);
                                            }
                                        }
                                        shipmentRoute.setBul010IdRouting(waypoint.getFacilityCode());
                                        shipmentRoute.setBul010IdNext(
                                                handlingUnit.getWaypoints().get(i + 1).getFacilityCode());
                                    }
                                    shipmentRouteList.add(shipmentRoute);
                                }
                            }
                            shipment.setShipmentRoute(shipmentRouteList);
                            shipment.setShipmentDetail(shipmentDetail);
                            shipment.setShipmentHeader(shipmentHeader);

                            listofshipments.add(shipment);

                        }
                    }
                }
            }
        }
        return listofshipments;
    }
}
