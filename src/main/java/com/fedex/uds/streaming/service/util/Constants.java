package com.fedex.uds.streaming.service.util;

public class Constants {

    public static final String SHIPMENT = "SHIPMENT";
    public static final String INTERCEPT = "INTERCEPT";
    public static final String ENTERPRISE = "ENTERPRISE";
    public static final String FEDEX = "FEDEX";
    public static final String EXPRESS = "EXPRESS";
    public static final String FRANCE_DOMESTIC = "FRANCE_DOMESTIC";
    public static final String TNT = "TNT";
    public static final String DESTINATION = "DESTINATION_FACILITY";
    public static final String ORIGIN = "ORIGIN_FACILITY";
    public static final String RECIPIENT = "RECIPIENT";
    public static final String ACTIVE = "ACTIVE";
    public static final String RELEASE = "RELEASE";
    public static final String SHIPPER = "SHIPPER";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String GLOBAL_INTERCEPT = "global-intercept";
    public static final String GEOGRAPHIC_INTERCEPT = "geographic-intercept";
    public static final String FACILITY_INTERCEPT = "facility-intercept";
    public static final String PROCEDURE_CODES = "procedure-codes";
    public static final String LOCATION_CODES = "location-codes";
    public static final String CLEARANCE = "clearance";
    public static final String TRADE_RESTRICTIONS = "trade-restriction";
    public static final String SINGLE_SPACE = " ";

}
