package com.fedex.uds.streaming.service.util;

import com.fedex.uds.common.dto.idm.MainShipment;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.stereotype.Service;

import java.io.StringWriter;

@Service
public class DataProvider {

    public String convertIdmMessage(MainShipment shipmentDto) throws Exception {
        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        serializer.write(shipmentDto, writer);
        return writer.toString();
    }
}
