package com.fedex.uds.streaming.service;

import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.common.dto.idm.*;
import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service("ShipmentIDMConversion")
public class ShipmentIDMConversion {

    @Autowired
    public HandlingUnitHandler handler;

    @Autowired
    private FormIdCheckProperties formIdCheck;

    @Autowired
    @Qualifier("TranslationMappingServiceInvoker")
    private TranslationMappingServiceInvoker mappingServiceInvoker;

    public MainShipment perforShipmentIdmConversion(HandlingUnit handlingUnit) throws Exception {
        /*
         * Get The values out of HandlingUnit and map to IDM DTOs to get the message in
         * IDM format for respective fields
         */
        // Sample Message for for Demo Purpose ONLY!!
        // Move this method in separate class

        try {
            MainShipment shipment = new MainShipment();

            SHP010 shipmentHeader = new SHP010();
            SHP170 shipmentDetail = new SHP170();

            ArrayList<SHP160> shipmentHandlingCodes = new ArrayList<SHP160>();

            // mapped

            if (handlingUnit != null) {
                List<String> orangeIds = formIdCheck.getFormId();
                Boolean orangePackage = orangeIds.contains(handlingUnit.getFormID());
                if (orangePackage) {
                    shipmentDetail.setPrdOwner(null);
                    shipmentDetail.setMasterTotalItemsNr("1");
                    if (handlingUnit.getShipment() != null) {
                        shipmentDetail.setSourceSystemCd(handlingUnit.getShipment().getOwningOpCo() != null ? handlingUnit.getShipment().getOwningOpCo() : " ");
                        if (handlingUnit.getShipment().getCustomerAssociations() != null && handlingUnit
                                .getShipment().getCustomerAssociations().get(0).getAlternateAccount() != null) {

                            shipmentHeader.setLegacyConNr(handlingUnit.getShipment().getCustomerAssociations()
                                    .get(0).getAlternateAccount().getId());
                        }

                    }


                    shipmentDetail.setCustomsClrIn("N");

                    workreq:
                    for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {

                        if (requirement.getType().equalsIgnoreCase("trade-restriction")) {
                            for (int i = 0; i < requirement.getAttributes().size(); i++) {
                                if (requirement.getAttributes().get(i).getKey().equalsIgnoreCase("trade-status")) {

                                    shipmentDetail.setCustomsClrIn("Y");
                                    break workreq;

                                }
                            }
                        }
                    }


                    // Something we have to set alternate id
                    // shipmentDetail.setMasterShipmentId(handlingUnit.getShipment().getMasterTrackingNumber());
                    if (handlingUnit.getHandlingUnitIds() != null && !(handlingUnit.getHandlingUnitIds().isEmpty())) {
                        handlingUnit.getHandlingUnitIds().forEach(handlinUnitId -> {
                            if (handlinUnitId.getIdType() != null && !(handlinUnitId.getIdType().isEmpty())) {
                                if (handlinUnitId.getIdType().equals("FRANCE_DOMESTIC")
                                        || handlinUnitId.getIdType().equals("TNT")) {
                                    shipmentHeader.setId(handlinUnitId.getId());
                                    shipmentDetail.setDomesticShipmentNr(handlinUnitId.getId());
                                    shipmentDetail.setDomesticShipmentType(handlinUnitId.getIdType());
                                }
                            }
                        });
                    }
                } else {
                    shipmentDetail.setPrdOwner("FX");
                    shipmentDetail.setSourceSystemCd("FX");
                    shipmentDetail.setDomesticShipmentNr("");
                    shipmentDetail.setDomesticShipmentType("");
                    if (handlingUnit.getShipment() != null) {
                        //shipmentDetail.setSourceSystemCd(handlingUnit.getShipment().getOwningOpCo());
                        shipmentDetail.setMasterShipmentId(handlingUnit.getShipment().getMasterTrackingNumber());
                        if (handlingUnit.getShipment().getTotalHU() != null) {
                            shipmentDetail.setMasterTotalItemsNr(handlingUnit.getShipment().getTotalHU().toString());
                        }
                    }

                }
                if (handlingUnit.getHandlingUnitIds() != null) {
                    shipmentHeader.setCustomerReferenceNr(handlingUnit.getHandlingUnitIds().stream()
                            .filter(handling -> handling.getIdType().equals(Constants.ENTERPRISE)
                                    || handling.getIdType().equalsIgnoreCase(Constants.FEDEX)
                                    || handling.getIdType().equalsIgnoreCase(Constants.EXPRESS))
                            .findAny().get().getId());
                }
                if (handlingUnit.getPhysicalCharacteristic() != null) {
                    if (handlingUnit.getPhysicalCharacteristic().getVolume() != null
                            && handlingUnit.getPhysicalCharacteristic().getVolume().getValue() != null) {
                        shipmentDetail.setActualVl(
                                handlingUnit.getPhysicalCharacteristic().getVolume().getValue().toString());
                    }
                    if (handlingUnit.getPhysicalCharacteristic().getWeight() != null
                            && handlingUnit.getPhysicalCharacteristic().getWeight().getValue() != null) {
                        shipmentDetail.setActualTotalWt(
                                handlingUnit.getPhysicalCharacteristic().getWeight().getValue().toString());
                    }
                }
                if (handlingUnit.getFormID() != null) {
                    shipmentDetail.setFormId(handlingUnit.getFormID());
                }

                shipmentDetail.setContractualTotalWt("0.00");
                shipmentDetail.setContractualVl("0.00");
                shipmentHeader.setCom010CdOrig(" ");
                shipmentHeader.setCom010CdDest(" ");


                if (handlingUnit.getWaypoints() != null && !(handlingUnit.getWaypoints().isEmpty())) {
                    handlingUnit.getWaypoints().forEach(waypoint -> {
                        SHP160 shipmentHandlingCode = new SHP160();
                        if (waypoint.getType() != null) {
                            if (waypoint.getType().equalsIgnoreCase(Constants.DESTINATION) && waypoint.getFacilityCode() != null) {
                                shipmentHandlingCode.setBul010IdOccuring(waypoint.getFacilityCode());
                                shipmentHeader.setBul010IdDest(waypoint.getFacilityCode());
                                if (shipmentHandlingCode != null) {
                                    shipmentHandlingCodes.add(shipmentHandlingCode);
                                }
                            }
                            if (waypoint.getType().equalsIgnoreCase(Constants.ORIGIN) && waypoint.getFacilityCode() != null) {
                                shipmentHeader.setBul010IdCollection(waypoint.getFacilityCode());
                                shipmentHeader.setBul010IdOrig(waypoint.getFacilityCode());
                            }
                            // shipmentHandlingCodes.add(shipmentHandlingCode);
                        } else {
                            shipmentHeader.setBul010IdOrig(" ");
                            shipmentHeader.setBul010IdDest(" ");
                            shipmentHeader.setBul010IdCollection(" ");
                        }


                    });

                }

                if (shipmentHandlingCodes != null && !(shipmentHandlingCodes.isEmpty())) {
                    shipment.setShipmentHandlingCode(shipmentHandlingCodes);
                }
                // mapped

                ArrayList<SHP880> shipmentRouteList = new ArrayList<SHP880>();
                if (handlingUnit.getWaypoints() != null && !(handlingUnit.getWaypoints().isEmpty())) {

                    for (int i = 0; i < handlingUnit.getWaypoints().size(); i++) {

                        SHP880 shipmentRoute = new SHP880();
                        Waypoint waypoint = handlingUnit.getWaypoints().get(i);
                        if ((i + 1) == handlingUnit.getWaypoints().size()) {
                            break;
                        } else {
                            shipmentRoute.setBul010IdRouting(waypoint.getFacilityCode());
                            shipmentRoute.setBul010IdNext(handlingUnit.getWaypoints().get(i + 1).getFacilityCode());
                            if (waypoint.getSequenceNumber() != null) {
                                if (shipmentHeader.getCustomerReferenceNr() != null) {
                                    shipmentRoute.setId(shipmentHeader.getCustomerReferenceNr() + "_"
                                            + waypoint.getSequenceNumber().toString());
                                }
                                shipmentRoute.setSequencingNr(waypoint.getSequenceNumber().toString());
                            }
                            shipmentRouteList.add(shipmentRoute);
                        }
                    }
                    shipment.setShipmentRoute(shipmentRouteList);
                }

                if (handlingUnit.getShipment() != null) {

                    shipmentHeader.setPaymentTermsCd("");

                    shipmentHeader.setId("");

                    shipmentHeader.setBul010IdClearance("");

                    // added as per Excel
                    if (handlingUnit.getCommitDate() != null) {
                        SimpleDateFormat dt = new SimpleDateFormat(Constants.DATE_FORMAT);
                        shipmentHeader.setDeliveryDueTd(dt.format(handlingUnit.getCommitDate()));
                    }
                    shipmentHeader.setPrd010Id(handlingUnit.getLegacyServiceCode());

                    ArrayList<ShipmentPartyDetail> partyDetail = new ArrayList<ShipmentPartyDetail>();

                    if (handlingUnit.getShipment() != null) {

                        if (handlingUnit.getShipment().getCustomerAssociations() != null
                                && !(handlingUnit.getShipment().getCustomerAssociations().isEmpty())) {
                            handlingUnit.getShipment().getCustomerAssociations().forEach(associations -> {

                                SHP030 shipmentPartyDetailHeader = new SHP030();
                                ACC010 account = new ACC010();
                                ADD010 address = new ADD010();
                                if (associations.getCustomerType() != null
                                        && !associations.getCustomerType().isEmpty()) {
                                    if (associations.getCustomerType().equals(Constants.RECIPIENT)) {

                                        shipmentPartyDetailHeader.setAddressFunctionTypeCd("R");
                                        // shipmentPartyDetailHeader.setAcc010Id(acc010Id);
                                        account.setNr(associations.getCustomerAccountnumber());

                                        account.setSrcSysCd(orangePackage ? "TNT" : "FX");

                                        if (associations.getCustomerAddress() != null) {
                                            address.setPostalAreaCd(associations.getCustomerAddress().getPostal());
                                            shipmentHeader.setCom010CdDest(
                                                    associations.getCustomerAddress().getCountryCode() != null ? associations.getCustomerAddress().getCountryCode() : " ");

                                        }

                                        partyDetail.add(
                                                new ShipmentPartyDetail(shipmentPartyDetailHeader, address, account));
                                    } else if (associations.getCustomerType().equals(Constants.SHIPPER)) {
                                        shipmentPartyDetailHeader.setAddressFunctionTypeCd("S");

                                        account.setNr(associations.getCustomerAccountnumber());
                                        account.setSrcSysCd(orangePackage ? "TNT" : "FX");
                                        if (associations.getCustomerAddress() != null)
                                            shipmentHeader.setCom010CdOrig(
                                                    associations.getCustomerAddress().getCountryCode() != null ? associations.getCustomerAddress().getCountryCode() : " ");

                                        partyDetail.add(
                                                new ShipmentPartyDetail(shipmentPartyDetailHeader, address, account));

                                    }
                                }
                            });
                        }
                        shipment.setShipmentPartyDetail(partyDetail);
                    }

                }

//	        shipmentDetail.setCustomsCtrlIn(handlingUnit.getCommodity().get(0).getclassification()));
                // For Pure Purple or P2O set ro "FX" else "TNT"

                shipmentDetail.setTotalItemsNr("1");

                shipmentDetail.setRnd010Id("");
                //shipmentDetail.setLastDepotSortDt(new Date());

                // shipmentDetail.setCustomsIn(handlingUnit.getCommodity());

                // setting details to shipment

                // shipment.setShipmentRoute(shipmentRouteList);

                MappingCodesDTO mappingCodesDTO = new MappingCodesDTO();
                List<UdsDGMappingEntity> udsDGMappingEntityList = new ArrayList<UdsDGMappingEntity>();
                UdsDGMappingEntity entity = new UdsDGMappingEntity();

                if (orangePackage) {
                    entity.setOperatingCompanyCd("TNT");
                } else {
                    entity.setOperatingCompanyCd("FX");
                }
                entity.setSourceDgTypeCd("06");
                udsDGMappingEntityList.add(entity);
                List<UdsServiceCodeMappingEntity> udsServiceCodeMappingEntityList = new ArrayList<UdsServiceCodeMappingEntity>();
                UdsServiceCodeMappingEntity serviceEntity = new UdsServiceCodeMappingEntity();

                serviceEntity.setServiceCd("RM"); // (handlingUnit.getLegacyServiceCode()); // hard code here for demo
                serviceEntity.setCommitTm("2359");
                udsServiceCodeMappingEntityList.add(serviceEntity);

                // Code Below Must be removed
                List<UdsHandlingCodeMappingEntity> udsHandlingCodeMappingEntityList = new ArrayList<UdsHandlingCodeMappingEntity>();
                UdsHandlingCodeMappingEntity udshandlingentity = new UdsHandlingCodeMappingEntity();
                udshandlingentity.setHandlingInterceptCd("CUD"); // hard coded and must be removed
                udsHandlingCodeMappingEntityList.add(udshandlingentity);

                mappingCodesDTO.setUdsDGMappingEntities(udsDGMappingEntityList);
                mappingCodesDTO.setUdsHandlingCodeMappingEntities(udsHandlingCodeMappingEntityList);
                mappingCodesDTO.setUdsServiceCodeMappingEntities(udsServiceCodeMappingEntityList);

                // revove till here

                MappingCodesDTO translationCodes = mappingServiceInvoker.getTranslationsResult(mappingCodesDTO);
                if (translationCodes != null) {

                    if ((translationCodes.getUdsDGMappingEntities().get(0) != null)) {
                        shipmentDetail.setDgType(translationCodes.getUdsDGMappingEntities().get(0).getUdsDgTypeCd());
                    }
                    if (translationCodes.getUdsServiceCodeMappingEntities() != null) {
                        shipmentDetail.setSpeedofService(
                                translationCodes.getUdsServiceCodeMappingEntities().get(0).getServiceCd());
                    }
                }

                shipment.setShipmentDetail(shipmentDetail);
                shipment.setShipmentHeader(shipmentHeader);
                return shipment;
            }
        } catch (Exception e) {
            log.info("Shipment IDM Mapping Exception: "+e.getMessage());
        }
        return null;
    }
}
