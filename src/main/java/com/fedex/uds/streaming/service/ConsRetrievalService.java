package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.bre.executorservice.domain.sortmodifier.model.BusinessRuleHandlingCodes;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.WorkAttribute;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.ConsRetreivalDTO;
import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.common.exception.ApplicationException;
import com.fedex.uds.common.exception.NoDataFoundException;
import com.fedex.uds.streaming.repository.PhuwvEventRepository;
import com.fedex.uds.streaming.repository.SortModifierRepository;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.phuwvevententity.entity.PhuwvEventsEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import com.fedex.uds.view.sortmodifierentity.entity.SortModifierServiceEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ConsRetrievalService")
@Slf4j
public class ConsRetrievalService {

    @Autowired
    ObjectMapper defaultObjectMapper;
    @Autowired
    private PhuwvEventRepository phuwvEventRepository;
    @Autowired
    private SortModifierRepository sortModifierRepository;
    @Autowired
    private TranslationMappingServiceInvoker translationMappingServiceInvoker;

    public ConsRetreivalDTO getLatestConsByTrknbr(String trknbr) throws Exception {
        List<UdsHandlingCodeMappingEntity> translatedList = new ArrayList<>();
        List<UdsHandlingCodeMappingEntity> sortModifierHandlingCodes = new ArrayList<>();
        List<String> handlingCodes = new ArrayList<>();
        ConsRetreivalDTO consData = new ConsRetreivalDTO();
        String sortModifierUUID = null;
        PhuwvEventsEntity phuwvEventsEntity = phuwvEventRepository
                .findFirstByEnterpriseHandlingUnitIdOrderByEventCreateTimestampDesc(trknbr);

        if (phuwvEventsEntity == null) {
            throw new NoDataFoundException("NO DATA FOUND");
        } else {
            HandlingUnit handlingUnit = null;

            log.debug("Creating json");
            String json = getJsonFromDataImg(phuwvEventsEntity);
            log.debug("Got json from entity ");
            log.debug(json);
            try {
                handlingUnit = defaultObjectMapper.readValue(json, HandlingUnit.class);
            } catch (Exception e) {
                log.error("Exception while mapping " + e.getMessage());
                throw new ApplicationException("" + e);
            }
            if (handlingUnit != null) {
                sortModifierUUID = handlingUnit.getHuUUID();
                sortModifierHandlingCodes = fetchHandlingCodesfromSortModifier(sortModifierUUID);
                if (sortModifierHandlingCodes != null && !sortModifierHandlingCodes.isEmpty()) {
                    sortModifierHandlingCodes.forEach(x -> {
                        if (x != null)
                            handlingCodes.add(x.getSorterInterceptCd());
                    });
                }
            } else {
                throw new NoDataFoundException("NO DATA FOUND");
            }

            translatedList = convertHandlingCodesType(handlingUnit);
            if (translatedList != null && !translatedList.isEmpty()) {
                translatedList.forEach(codes -> {
                    if (codes != null)
                        handlingCodes.add(codes.getSorterInterceptCd());
                });
            }
            //consData.setIntercepts(handlingCodes);
            consData.setIntercepts(handlingCodes.stream().distinct().collect(Collectors.toList()));
        }
        return consData;
    }

    public ConsRetreivalDTO getLatestConsByPhuvwJson(HandlingUnit phuwvJson) {
        ConsRetreivalDTO info = new ConsRetreivalDTO();
        if (phuwvJson != null && phuwvJson.getType() != null && phuwvJson.getType().equalsIgnoreCase("CONSOLIDATION")) {
            List<UdsHandlingCodeMappingEntity> entities = new ArrayList<>();
            if (phuwvJson.getWorkRequirements() != null) {
                phuwvJson.getWorkRequirements().forEach(requirement -> {
                    entities.addAll(getUdsHandlingCodeMappingEntities(requirement));
                });
            }
            info.setIntercepts(getInterceptsFromTranslationMapping(entities));
            return info;
        } else {
            throw new NoDataFoundException("NO DATA FOUND");
        }

    }

    private List<UdsHandlingCodeMappingEntity> fetchHandlingCodesfromSortModifier(String sortModifierUUID)
            throws IOException {
        SortModifierServiceEntity sortModifierEntity = sortModifierRepository
                .findFirstBySortModifierServiceId_handlingUnitUUIDOrderByEventCreateTimestampDesc(sortModifierUUID);
        List<UdsHandlingCodeMappingEntity> translatedList = new ArrayList<>();
        if (sortModifierEntity == null) {
            throw new NoDataFoundException("No DATA FOUND");
        } else {
            BusinessRuleHandlingCodes breCodes = null;

            log.debug("Creating json");
            byte[] dataImage1 = sortModifierEntity.getDataImg1();
            byte[] dataImage2 = sortModifierEntity.getDataImg2();
            if ((null != dataImage1) || (null != dataImage2)) {
                String json = BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
                log.debug("Got json from entity ");
                log.debug(json);
                breCodes = defaultObjectMapper.readValue(json, BusinessRuleHandlingCodes.class);
            }

            if (breCodes != null) {
                translatedList = convertHandlingCodesTypeFromBRE(breCodes);
            }

        }
        return translatedList;
    }

    private List<UdsHandlingCodeMappingEntity> convertHandlingCodesTypeFromBRE(BusinessRuleHandlingCodes breCodes) {
        List<UdsHandlingCodeMappingEntity> handlingCodeList = new ArrayList<>();
        if (breCodes != null && breCodes.getLocationHandlingCodes() != null && !breCodes.getLocationHandlingCodes().isEmpty()) {
            breCodes.getLocationHandlingCodes().forEach(locationCode -> {
                if (locationCode.getHandlingCodes() != null && !locationCode.getHandlingCodes().isEmpty()) {
                    locationCode.getHandlingCodes().forEach(handlingCode -> {
                        UdsHandlingCodeMappingEntity udsHandlingCodeMappingEntity = new UdsHandlingCodeMappingEntity();
                        udsHandlingCodeMappingEntity.setHandlingInterceptCd(handlingCode.getHandlingCode().substring(1, handlingCode.getHandlingCode().length() - 1));
                        handlingCodeList.add(udsHandlingCodeMappingEntity);
                    });
                }
            });
        }
        return translationMappingServiceInvoker
                .getTranslationsResult(getMappingCodes(handlingCodeList)).getUdsHandlingCodeMappingEntities();
    }

    private List<UdsHandlingCodeMappingEntity> convertHandlingCodesType(HandlingUnit handlingUnit) {
        List<UdsHandlingCodeMappingEntity> translatedList = new ArrayList<>();
        if (handlingUnit.getType() != null
                && handlingUnit.getType().equalsIgnoreCase("CONSOLIDATION")) {
            for (WorkRequirement workRequirement : handlingUnit.getWorkRequirements()) {
                translatedList.addAll(getUdsHandlingCodeMappingEntities(workRequirement));
            }
        }
        return translationMappingServiceInvoker.getTranslationsResult(getMappingCodes(translatedList)).getUdsHandlingCodeMappingEntities();
    }

    public String getJsonFromDataImg(PhuwvEventsEntity entity) throws IOException {
        byte[] dataImage1 = entity.getDataImg1();
        byte[] dataImage2 = entity.getDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }

        return null;
    }

    private List<UdsHandlingCodeMappingEntity> getUdsHandlingCodeMappingEntities(WorkRequirement requirement) {
        List<UdsHandlingCodeMappingEntity> entities = new ArrayList<>();
        if (requirement.getType() != null
                && (requirement.getType().equalsIgnoreCase("global-intercept")
                || requirement.getType().equalsIgnoreCase("geographic-intercept"))
                && !requirement.getAttributes().isEmpty()) {
            for (WorkAttribute workAttribute : requirement.getAttributes()) {
                if (workAttribute.getKey().equalsIgnoreCase("procedure-codes") && workAttribute.getValues() != null
                        && !workAttribute.getValues().isEmpty()) {
                    workAttribute.getValues().forEach(value -> {
                        UdsHandlingCodeMappingEntity entityForValue = new UdsHandlingCodeMappingEntity();
                        entityForValue.setHandlingInterceptCd(value);
                        entities.add(entityForValue);
                    });
                }
            }
        }
        return entities;
    }

    private List<String> getInterceptsFromTranslationMapping(List<UdsHandlingCodeMappingEntity> entities) {

        MappingCodesDTO translatedResult = translationMappingServiceInvoker.getTranslationsResult(getMappingCodes(entities));
        List<String> intercepts = new ArrayList<>();
        if (translatedResult != null && !translatedResult.getUdsHandlingCodeMappingEntities().isEmpty()) {
            translatedResult.getUdsHandlingCodeMappingEntities().forEach(translationEntity -> {
                if (translationEntity != null)
                    intercepts.add(translationEntity.getSorterInterceptCd());
            });
        }
        //return intercepts;
        return intercepts.stream().distinct().collect(Collectors.toList());

    }

    private MappingCodesDTO getMappingCodes(List<UdsHandlingCodeMappingEntity> entities) {
        MappingCodesDTO mappingCodes = new MappingCodesDTO();
        mappingCodes.setUdsHandlingCodeMappingEntities(entities);
        List<UdsDGMappingEntity> dgEntities = new ArrayList<>();
        List<UdsServiceCodeMappingEntity> serviceEntities = new ArrayList<>();
        mappingCodes.setUdsDGMappingEntities(dgEntities);
        mappingCodes.setUdsServiceCodeMappingEntities(serviceEntities);
        return mappingCodes;
    }

}
