package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.uds.streaming.repository.PhuwvEventRepository;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.view.phuwvevententity.entity.PhuwvEventsEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service("PhuwvEventsService")
@Slf4j
public class PhuwvEventsService {
    @Autowired
    PhuwvEventRepository phuwvEventRepository;

    @Autowired
    ObjectMapper defaultObjectMapper;

    public HandlingUnit getPhuwvEventsEntityByHandlingUnitUUID(String handlingUnitUUID)
            throws IOException {
        PhuwvEventsEntity entity = phuwvEventRepository.findTopByHandlingUnitId_handlingUnitUUIDOrderByEventCreateTimestampDesc(handlingUnitUUID);

        HandlingUnit handlingUnit = null;
        if (entity != null) {
            log.debug("Creating json");
            String json = getJsonFromDataImg(entity);
            log.debug("Got json from entity ");
            log.debug(json);
            handlingUnit = defaultObjectMapper.readValue(json, HandlingUnit.class);
        } else
            log.info("No PhuwvEventsEntity found for handlingUnitUUID and for eventUUID");
        return handlingUnit;
    }

    public String getJsonFromDataImg(PhuwvEventsEntity entity) throws IOException {
        // Update as data columns are added
        String retString = null;
        byte[] dataImage1 = entity.getDataImg1();
        byte[] dataImage2 = entity.getDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }

        return retString;
    }
}
