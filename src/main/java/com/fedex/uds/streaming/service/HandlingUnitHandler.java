package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.idm.MainShipment;
import com.fedex.uds.common.exception.ApplicationException;
import com.fedex.uds.common.exception.ResourceException;
import com.fedex.uds.common.exception.RetryableException;
import com.fedex.uds.common.exception.classifier.ExceptionClassifier;
import com.fedex.uds.common.jms.JMSPublisher;
import com.fedex.uds.common.jms.MessageHandler;
import com.fedex.uds.common.logging.Audit;
import com.fedex.uds.common.logging.AuditKey;
import com.fedex.uds.common.types.StatusTypes;
import com.fedex.uds.common.validator.JsonValidator;
import com.fedex.uds.streaming.configuration.DeltaCheckProperties;
import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
import com.fedex.uds.streaming.configuration.UdsPhuwvCollectorProperties;
import com.fedex.uds.streaming.repository.PhuwvEventRepository;
import com.fedex.uds.streaming.repository.UdsInterceptPublishStatusRepository;
import com.fedex.uds.streaming.repository.UdsShipmentPublishStatusRepository;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.util.CompressionUtil;
import com.fedex.uds.util.JsonUtil;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusId;
import com.fedex.uds.view.phuwvevententity.entity.PhuwvEventsEntity;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.ValueChange;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service("HandlingUnitHandler")
@Slf4j
public class HandlingUnitHandler implements MessageHandler<HandlingUnit> {
    boolean deltaCheckShipment = false;
    boolean deltaCheckIntercept = false;
    private ExceptionClassifier<RetryableException> exceptionClassifier;
    private ObjectMapper objectMapper;
    private RetryTemplate retryTemplate;
    private JsonValidator jsonValidator;
    @Autowired
    private UdsShipmentPublishStatusService udsShipmentPublishStatusService;
    @Autowired
    private UdsInterceptPublishStatusService udsInterceptPublishStatusService;

    @Autowired
    // @Qualifier("udsIntercept")
    private PlatformTransactionManager transactionManager;

    @Autowired
    private JMSPublisher handlingUnitPublisher;

    @Autowired
    private DeltaCheckProperties deltacheck;
    @Autowired
    @Qualifier("ShipmentIDMConversion")
    private ShipmentIDMConversion shipmentIDMConversion;

    @Autowired
    private InterceptIDMConversion interceptIDMConversion;

    @Autowired
    private UdsShipmentPublishStatusRepository shipemtRepository;

    @Autowired
    private UdsInterceptPublishStatusRepository interceptRepository;

    private UdsPhuwvCollectorProperties udsPhuwvCollectorProperties;

    @Autowired
    private FormIdCheckProperties formIdCheck;

    @Autowired
    private ShipmentIDM shipmentIDM;

    @Autowired
    public HandlingUnitHandler(ExceptionClassifier<RetryableException> exceptionClassifier,
                               @Qualifier("defaultObjectMapper") ObjectMapper objectMapper, PhuwvEventRepository repository,
                               @Qualifier("jmsRetryTemplate") RetryTemplate retryTemplate,
                               @Qualifier("jsonValidator") JsonValidator jsonValidator,
                               UdsPhuwvCollectorProperties udsPhuwvCollectorProperties) {

        this.udsPhuwvCollectorProperties = udsPhuwvCollectorProperties;
        this.exceptionClassifier = exceptionClassifier;
        this.objectMapper = objectMapper;
        this.retryTemplate = retryTemplate;
        this.jsonValidator = jsonValidator;
        log.info("HandlingUnitHandler initialized");
    }

    @Override
    public void handle(GenericMessage<HandlingUnit> genericMessage) throws ResourceException, Exception {
        retryTemplate.execute(retryContext -> {
            Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitHandler.handle");
            DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
            txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            TransactionStatus transactionStatus = null;
            transactionStatus = transactionManager.getTransaction(txDef);

            try {
                HandlingUnit handlingUnit = genericMessage.getPayload();
                if (handlingUnit.getType() != null && !(handlingUnit.getType().isEmpty())
                        && handlingUnit.getType().equalsIgnoreCase(Constants.SHIPMENT)) {
                    String eventUUId = (String) genericMessage.getHeaders().get("jms_correlationId");

                    log.info("HandlingUnit: {}", handlingUnit.toString().replaceAll("[\\n]", ""));
                    log.info("eventUUId: {}", eventUUId);

                    if (log.isDebugEnabled()) {
                        try {
                            String json = JsonUtil.asCompactJson(objectMapper, handlingUnit);
                            log.debug("HandlingUnitPayload: {}", json);
                        } catch (Exception e) {
                            // ignore
                        }
                    }
                    jsonValidator.validate(handlingUnit);
                    log.info("Checking if package Orange or Purple");
                    List<String> orangeIds = formIdCheck.getFormId();

                    Boolean orangePackage = orangeIds.contains(handlingUnit.getFormID());
                    HandlingUnitId handlingUnitId = handlingUnit.getHandlingUnitIds().stream()
                            .filter(s -> s.getIdType().equalsIgnoreCase("ENTERPRISE")
                                    || s.getIdType().equalsIgnoreCase("FEDEX")
                                    || s.getIdType().equalsIgnoreCase("EXPRESS"))
                            .findAny().orElse(null);
                    
                    String masterTrackingNumber = handlingUnit.getShipment()!=null? handlingUnit.getShipment().getMasterTrackingNumber():null;
                    if (handlingUnitId != null) {
                        Boolean masterPiece = handlingUnitId.getId()!=null?handlingUnitId.getId().equalsIgnoreCase(masterTrackingNumber):false;
                        if ((orangePackage && masterPiece) || (!orangePackage && masterPiece)
                                || (!orangePackage && !masterPiece)) {

                            log.info(" --- calling shipment idm conversion --- ");

                            // Shipment IDM Conversion
                            shipmentIDM.shipmentIdmLogic(handlingUnit, eventUUId);

                            // intercept IDM Conversion
                            log.info("IDM intercept conversion begins...");
                            List<MainShipment> interceptDtos = interceptIDMConversion
                                    .performInterceptIdmConversion(handlingUnit);
                            if (interceptDtos != null) {
                                List<String> convertInterceptIdmMessage = convertInterceptIdmMessage(interceptDtos);
                                log.info("IDM_Intercept: {} ",
                                        convertInterceptIdmMessage.toString().replaceAll("[\\n]", "")); // needs rework
                                log.info("Delta check for Intercept for more triggers ");
                                performDeltaCheckForIntercept(interceptDtos, handlingUnit, eventUUId,
                                        transactionStatus);
                            }
                        }
                        transactionManager.commit(transactionStatus);
                        log.info("Transaction Committed for Intercept");
                    }

                }
                return null;
            } catch (Throwable e) {
                if (transactionStatus != null && !transactionStatus.isCompleted()) {
                    transactionManager.rollback(transactionStatus);
                    log.info("Transaction rollbacked");
                }
                throw e;
            }
        }, retryContext -> {
            RetryableException retryableException = null;
            try {
                retryableException = exceptionClassifier.classify(retryContext.getLastThrowable());
            } catch (Throwable e) {
                throw new ApplicationException("Failed to recover: " + ExceptionUtils.getFullStackTrace(e), e);
            }

            if (retryableException != null) {
                String json = null;
                try {
                    // json = JsonUtil.asCompactJson(objectMapper, message.toString());
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
                throw new ResourceException(
                        "Failed to process domain event: " + json + " due to " + retryableException.getMessage(),
                        retryableException);
            }

            return null;
        });
    }

    private String convertIdmMessage(MainShipment shipmentDto) throws Exception {
        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        serializer.write(shipmentDto, writer);
        return writer.toString();
    }

    private List<String> convertInterceptIdmMessage(List<MainShipment> interceptDto) throws Exception {
        List<String> listofIDMMessage = new ArrayList<>();
        for (MainShipment mainshipment : interceptDto) {

            String idmMessage = convertIdmMessage(mainshipment);

            listofIDMMessage.add(idmMessage);
            // System.out.println(idmMessage);
        }
        return listofIDMMessage;
    }

    private boolean performDeltaCheckForYesFields(MainShipment shipmentIdm, MainShipment fromUdsPublishStatus,
                                                  List<String> deltaCheckYesField) {
        boolean deltaChanged = false;
        Javers javers = JaversBuilder.javers().build();
        Diff diff = javers.compare(shipmentIdm, fromUdsPublishStatus);
        List<ValueChange> graph = diff.getChangesByType(ValueChange.class).stream().collect(Collectors.toList());
        for (int i = 0; i < graph.size(); i++) {
            deltaChanged = deltaCheckYesField.contains(graph.get(i).getPropertyNameWithPath().replaceAll("/\\//g.", "")
                    .replaceAll("\\/", ".").replaceAll("\\d\\.", ""));
            if (deltaChanged)
                break;
        }

        return deltaChanged;

    }

    boolean performDeltaCheckForShipment(MainShipment shipmentIdm, MainShipment fromUdsPublishStatus) {

        boolean deltaChanged;
        List<String> deltaCheckYesField = deltacheck.getShipment().entrySet().stream()
                .filter(e -> e.getValue().equals('Y')).map(Map.Entry::getKey).collect(Collectors.toList());
        deltaChanged = performDeltaCheckForYesFields(shipmentIdm, fromUdsPublishStatus, deltaCheckYesField);
        return deltaChanged;
    }

    public boolean performDeltaCheckForIntercept(List<MainShipment> interceptDtos, HandlingUnit handlingUnit,
                                                 String eventUUID, TransactionStatus transactionStatus) throws IOException {
        List<String> udsInterceptCd = new ArrayList<>();
        List<String> primaryInterceptLocationCd = new ArrayList<>();
        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
        	 if (requirement.getType() != null &&(requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)
                     || requirement.getType().equalsIgnoreCase(Constants.GEOGRAPHIC_INTERCEPT)
                     || requirement.getType().equalsIgnoreCase(Constants.FACILITY_INTERCEPT)))
             {
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("clnLocCd")) {
                    String trackLocCd = requirement.getAdditionalAttributes().get(i).getValues().get(0);
                    primaryInterceptLocationCd.add(trackLocCd);
                    break;
                }
            }
        }
        }
        
        
		interceptDtos.forEach(listofhu -> {

			udsInterceptCd.add(listofhu.getShipmentHandlingCode().get(0).getHac010Id());

		});


        List<MainShipment> fromUdsInterceptPublishStatus = udsInterceptPublishStatusService
                .getLatestUdsInterceptPublishStatusByHandlingUnitUUIDAndUdsInterceptCodeAndPrimaryInterceptLocationCode(
                        handlingUnit.getHuUUID(), udsInterceptCd, primaryInterceptLocationCd);
        interceptDtos.forEach(interceptDto -> {
            MainShipment fromUdsInterceptPublish = fromUdsInterceptPublishStatus.stream()
                    .filter(intercept -> interceptDto.getShipmentHandlingCode().get(0).getHac010Id()
                            .equals(intercept.getShipmentHandlingCode().get(0).getHac010Id()))
                    .findAny().orElse(null);
            if (fromUdsInterceptPublish == null) {
                // msg with new uds_intercept_cd directly publish and persist
                try {
                    String interceptIdmMessage = convertIdmMessage(interceptDto);
                    updateInterceptPublsihStatus(interceptIdmMessage, Constants.INTERCEPT, handlingUnit, eventUUID);
                    // needs to commit before publish
                    publishUDSEvent(interceptIdmMessage, Constants.INTERCEPT,
                            getHeaderDetailIntercept(interceptIdmMessage, handlingUnit));
                    log.info("First PhuwvIntercept Message: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+getHeaderDetailIntercept(interceptIdmMessage, handlingUnit)+" "+"MessageBody: "+interceptIdmMessage.replaceAll("[\\n]", ""));

                    Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
                } catch (Exception e) {
                    log.error("" + e);
                }

            } else {
                try {
                    Serializer serializer = new Persister();
                    String interceptIdmMessage = convertIdmMessage(interceptDto);
                    MainShipment interceptIdm = serializer.read(MainShipment.class,
                            interceptIdmMessage.replace("\"\"", "null"));
                    // Delta Check For intercept
                    List<String> deltaCheckYesField = deltacheck.getIntercept().entrySet().stream()
                            .filter(e -> e.getValue().equals('Y')).map(Map.Entry::getKey).collect(Collectors.toList());
                    deltaCheckIntercept = performDeltaCheckForYesFields(interceptIdm, fromUdsInterceptPublish,
                            deltaCheckYesField);

                    if (deltaCheckIntercept) {
                        updateInterceptPublsihStatus(interceptIdmMessage, Constants.INTERCEPT, handlingUnit, eventUUID);

                        log.info("Intercept Completed");
                        publishUDSEvent(interceptIdmMessage, Constants.INTERCEPT,
                                getHeaderDetailIntercept(interceptIdmMessage, handlingUnit));
                        log.info("After Delta PhuwvIntercept: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+getHeaderDetailIntercept(interceptIdmMessage, handlingUnit)+" "+"MessageBody: "+interceptIdmMessage.replaceAll("[\\n]", ""));
                        Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
                    }
                } catch (Exception e) {
                    log.error("Error during Intercept Delta" + e.getMessage());
                }
            }
        });
        return deltaCheckIntercept;
    }

    public String getJsonFromDataImg(PhuwvEventsEntity entity) throws IOException {
        // Update as data columns are added
        byte[] dataImage1 = entity.getDataImg1();
        byte[] dataImage2 = entity.getDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }
        return null;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void updateInterceptPublsihStatus(String xmlString, String shipmentOrIntercept, HandlingUnit handlingUnit,
                                             String eventUUID) throws Exception {

        UdsInterceptPublishStatusEntity interceptEntity = new UdsInterceptPublishStatusEntity();
        interceptEntity.setUdsInterceptPublishStatusId(
                new UdsInterceptPublishStatusId(handlingUnit.getHuUUID(), Instant.now()));

        if (handlingUnit.getHandlingUnitIds() != null && handlingUnit.getHandlingUnitIds().size() > 0) {

            HandlingUnitId handlingUnitId = handlingUnit.getHandlingUnitIds().get(0);

            if (handlingUnitId.getIdType().equalsIgnoreCase("ENTERPRISE")
                    || handlingUnitId.getIdType().equalsIgnoreCase("FEDEX")
                    || handlingUnitId.getIdType().equalsIgnoreCase("EXPRESS")) {

                interceptEntity.setEnterpriseHandlingUnitId(handlingUnitId.getId());

            } else {
                interceptEntity.setTntHandlingUnitId(handlingUnitId.getId());
            }

        } else {
            interceptEntity.setEnterpriseHandlingUnitId(null);
            interceptEntity.setTntHandlingUnitId(null);
        }
        // interceptEntity.setUdsInterceptCode("HVL");
        interceptEntity.setPhuwvEventUUID(eventUUID);
        interceptEntity.setRowPurgeDt(getDefaultPurgeDate());

        List<Waypoint> waypointsCountryCodes = handlingUnit.getWaypoints() != null
                ? handlingUnit.getWaypoints().stream().filter(waypoint -> waypoint.getFacilityCountryCode() != null)
                .collect(Collectors.toList())
                : null;
        List<String> countryCodes = waypointsCountryCodes != null
                ? waypointsCountryCodes.stream().map(Waypoint::getFacilityCountryCode).collect(Collectors.toList())
                : null;
        interceptEntity.setRoutingCountryCode(
                countryCodes != null ? String.join(",", countryCodes.stream().distinct().collect(Collectors.toList()))
                        : null);

        String trackLocCd = null;
        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
        	if (requirement.getType() != null &&(requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)
                    || requirement.getType().equalsIgnoreCase(Constants.GEOGRAPHIC_INTERCEPT)
                    || requirement.getType().equalsIgnoreCase(Constants.FACILITY_INTERCEPT)))
            {
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("clnLocCd")) {
                    trackLocCd = requirement.getAdditionalAttributes().get(i).getValues().get(0);
                    break;
                }
            }
        }
        }
        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
            // List<String> routingLocTxt =null;
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                List<String> listroutingLocTxt = null;
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("locId")) {
                    listroutingLocTxt = requirement.getAdditionalAttributes().get(i).getValues();
                    interceptEntity.setRoutingLocationsTxt(
                            String.join(",", listroutingLocTxt.stream().distinct().collect(Collectors.toList())));
                }

            }
            // listroutingLocTxt.add(routingLocTxt);

        }

        interceptEntity.setPrimaryInterceptLocationCode(trackLocCd);

        // interceptEntity.setPrimaryInterceptLocationCode(eventUUID);
        // handlingUnit.getWorkRequirements().get(0).get // waiting for 2.1.2
        // interceptEntity.setPublishedCountNbr((interceptRepository.count()) + 1);

        Serializer serializer = new Persister();
        MainShipment interceptIdm = serializer.read(MainShipment.class, xmlString);
        if (interceptIdm.getShipmentHandlingCode() != null && interceptIdm.getShipmentHandlingCode().get(0) != null)
            interceptEntity.setUdsInterceptCode((interceptIdm.getShipmentHandlingCode().get(0).getHac010Id()));
        if (interceptIdm.getShipmentHandlingCode() != null && interceptIdm.getShipmentHandlingCode().get(0) != null)
            if (interceptIdm.getShipmentHandlingCode().get(0).getGlobalCd().equalsIgnoreCase("Y")) {
                interceptEntity.setMessageSubTypeCode("G");
            } else {
                interceptEntity.setMessageSubTypeCode("L");
            }
        convertXmlStringToRaw(xmlString, interceptEntity);
        interceptRepository.save(interceptEntity);
//        transactionManager.commit(getTrxStatus());
    }

    public Date getDefaultPurgeDate() {
        Instant now = Instant.now();
        Instant purgeInstant = now.plus(udsPhuwvCollectorProperties.getMaxRetentionDays(), ChronoUnit.DAYS);
        Date date = Date.from(purgeInstant);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private void convertXmlStringToRaw(String compactJson, UdsShipmentPublishStatusEntity entity) throws IOException {
        List<byte[]> dataImages = getDataImages(compactJson);
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        int dataImageToSet = dataImages.size();
        entity.setShipmentDataImg1(dataImages.get(0));
        if (dataImages.size() > 1) {
            entity.setShipmentDataImg2(dataImages.get(1));
        }
        if (dataImageToSet > 2) {
            log.error("Data Image Size={}", dataImageToSet);
            log.error("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        }
    }

    private void convertXmlStringToRaw(String compactJson, UdsInterceptPublishStatusEntity entity) throws IOException {
        List<byte[]> dataImages = getDataImages(compactJson);
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        int dataImageToSet = dataImages.size();
        entity.setInterceptDataImg1(dataImages.get(0));
        if (dataImages.size() > 1) {
            entity.setInterceptDataImg2(dataImages.get(1));
        }
        if (dataImageToSet > 2) {
            log.error("Data Image Size={}", dataImageToSet);
            log.error("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        }

    }

    private List<byte[]> getDataImages(String compactJson) throws IOException {

        int maxColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumnLength();
        int totalColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumn() * maxColumnLength;
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        log.info("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
        return dataImages;
    }

    public void publishUDSEvent(String xmlContent, String shipmentOrIntercept, Map<String, String> headers) {

        handlingUnitPublisher.publish(xmlContent, new MessagePostProcessor() {
            public Message postProcessMessage(Message message) throws JMSException {
                if (shipmentOrIntercept.equals(Constants.SHIPMENT)) {
                    log.info("IDM_Shipment_JMSProperties: {}", headers.toString().replaceAll("[\\n]", ""));
                    log.info("IDM_Shipment_Message: {}", message.toString().replaceAll("[\\n]", ""));
                    setIDMheaders(message, headers);
                    return message;
                } else {
                    log.info("IDM_Intercept_JMSProperties: {}", headers.toString().replaceAll("[\\n]", ""));
                    log.info("IDM_Intercept_Message: {}", message.toString().replaceAll("[\\n]", ""));
                    setIDMheaders(message, headers);
                    return message;
                }

            }
        });

    }

    private Map<String, String> getHeaderDetailIntercept(String interceptMessage, HandlingUnit handlingUnit)
            throws Exception {
        Serializer serializer = new Persister();
        MainShipment interceptIdm = serializer.read(MainShipment.class, interceptMessage);
        String globalCd = null;
        if (interceptIdm.getShipmentHandlingCode() != null && interceptIdm.getShipmentHandlingCode().get(0) != null)
            globalCd = interceptIdm.getShipmentHandlingCode().get(0).getGlobalCd();
        String bul010IdRequested = interceptIdm.getShipmentDetail().getBul010IdRequested();
        String opCo = interceptIdm.getShipmentDetail().getSourceSystemCd();
        String barcdType = interceptIdm.getShipmentDetail().getBarCdType();

        Map<String, String> jmsHeaders = new HashMap<>();

        if (barcdType == null && bul010IdRequested != null) {
            jmsHeaders.put("DeviceType", "UNICONS");
        } else if (barcdType != null && bul010IdRequested != null) {
            jmsHeaders.put("DeviceType", "UDS");
        }
        jmsHeaders.put("MsgSource", "UDSAdaptor");
        // need to implement logic
        jmsHeaders.put("GeoCode", Constants.SINGLE_SPACE);
        if (opCo != null) {
            jmsHeaders.put("OpCo", opCo.equalsIgnoreCase("FX") ? "FDX" : "TNT");
        }
        jmsHeaders.put("MessageCategoryCd", "IC");
        jmsHeaders.put("FormCd", interceptIdm.getShipmentDetail().getFormId() != null ? handlingUnit.getFormID() : " ");
        jmsHeaders.put("ServiceCd", Constants.SINGLE_SPACE);
        jmsHeaders.put("MsgVsn", "1");
        jmsHeaders.put("MsgCharCd", "XML");
        if (interceptIdm.getShipmentHeader().getCom010CdOrig() != null) {
            jmsHeaders.put("ORIGIN_COUNTRY_CODE", interceptIdm.getShipmentHeader().getCom010CdOrig());
        }
        if (interceptIdm.getShipmentHeader().getBul010IdCollection() != null) {
            jmsHeaders.put("OrigLocationCd", interceptIdm.getShipmentHeader().getBul010IdCollection());
        }
        if (interceptIdm.getShipmentHeader().getCom010CdDest() != null) {
            jmsHeaders.put("DEST_COUNTRY_CODE", interceptIdm.getShipmentHeader().getCom010CdDest());
        }
        if (interceptIdm.getShipmentHeader().getBul010IdDest() != null) {
            jmsHeaders.put("DestLocationCd", interceptIdm.getShipmentHeader().getBul010IdDest());
        }
        // Derive from addtionalAtrributes(2.1.2) locId values
        jmsHeaders.put("RoutingLoc", Constants.SINGLE_SPACE);
        if (interceptIdm.getShipmentHandlingCode() != null && !(interceptIdm.getShipmentHandlingCode().isEmpty())) {
            jmsHeaders.put("SpecialHandlingCd", interceptIdm.getShipmentHandlingCode().get(0).getHac010Id());
        }
        // Derive from addtionalAtrributes(2.1.2) clnLocCd values get(0)
        jmsHeaders.put("TrackLocCd", Constants.SINGLE_SPACE);
        jmsHeaders.put("MessageType", "A");
        if (globalCd != null) {
            jmsHeaders.put("MessageSubTypeCd", globalCd.equalsIgnoreCase("Y") ? "G" : "L");
        }
        jmsHeaders.put("TriggerEventType", "IC");
        jmsHeaders.put("CARRIER_OID", "FDX");
        // unique Routing Country codes
        List<Waypoint> waypointsCountryCodes = handlingUnit.getWaypoints() != null
                ? handlingUnit.getWaypoints().stream().filter(waypoint -> waypoint.getFacilityCountryCode() != null)
                .collect(Collectors.toList())
                : null;
        List<String> countryCodes = waypointsCountryCodes != null
                ? waypointsCountryCodes.stream().map(Waypoint::getFacilityCountryCode).collect(Collectors.toList())
                : null;
        jmsHeaders.put("RoutingCountryCd",
                countryCodes != null ? String.join(",", countryCodes.stream().distinct().collect(Collectors.toList()))
                        : null);

        return jmsHeaders;

    }

    Map<String, String> getHeaderDetailShipment(MainShipment shipmentMessage, HandlingUnit handlingUnit)
            throws Exception {

        Map<String, String> jmsHeaders = new HashMap<>();
        String bul010IdRequested = shipmentMessage.getShipmentDetail().getBul010IdRequested();
        String opCo = shipmentMessage.getShipmentDetail().getSourceSystemCd();
        String barcdType = shipmentMessage.getShipmentDetail().getBarCdType();
        if (barcdType == null && bul010IdRequested != null) {
            jmsHeaders.put("DeviceType", "UNICONS");
        } else if (barcdType != null && bul010IdRequested != null) {
            jmsHeaders.put("DeviceType", "UDS");
        }
        jmsHeaders.put("MsgSource", "UDSAdaptor");
        // need to implement logic
        jmsHeaders.put("GeoCode", Constants.SINGLE_SPACE);
        if (opCo != null) {
            jmsHeaders.put("OpCo", opCo.equalsIgnoreCase("FX") ? "FDX" : "TNT");
        }
        jmsHeaders.put("MessageCategoryCd", "SH");
        jmsHeaders.put("FormCd",
                shipmentMessage.getShipmentDetail().getFormId() != null
                        ? shipmentMessage.getShipmentDetail().getFormId()
                        : Constants.SINGLE_SPACE);
        jmsHeaders.put("ServiceCd",
                shipmentMessage.getShipmentDetail().getSpeedofService() != null
                        ? shipmentMessage.getShipmentDetail().getSpeedofService()
                        : Constants.SINGLE_SPACE);
        jmsHeaders.put("MsgVsn", "1");
        jmsHeaders.put("MsgCharCd", "XML");
        if (shipmentMessage.getShipmentHeader().getCom010CdOrig() != null) {
            jmsHeaders.put("ORIGIN_COUNTRY_CODE", shipmentMessage.getShipmentHeader().getCom010CdOrig());
        }
        if (shipmentMessage.getShipmentHeader().getBul010IdCollection() != null) {
            jmsHeaders.put("OrigLocationCd", shipmentMessage.getShipmentHeader().getBul010IdCollection());
        }
        if (shipmentMessage.getShipmentHeader().getCom010CdDest() != null) {
            jmsHeaders.put("DEST_COUNTRY_CODE", shipmentMessage.getShipmentHeader().getCom010CdDest());
        }
        if (shipmentMessage.getShipmentHeader().getBul010IdDest() != null) {
            jmsHeaders.put("DestLocationCd", shipmentMessage.getShipmentHeader().getBul010IdDest());
        }
        StringBuilder rountingLoc = new StringBuilder();
        if (handlingUnit.getWaypoints() != null) {
            handlingUnit.getWaypoints().forEach(wayPoint -> {
                if (!wayPoint.getType().equalsIgnoreCase("ORIGIN")
                        && !wayPoint.getType().equalsIgnoreCase("DESTINATION")) {
                    if (wayPoint.getType().equalsIgnoreCase("ORIGIN_FACILITY")
                    && wayPoint.getFacilityCode() != null) {
                        rountingLoc.append(wayPoint.getFacilityCode());
                    }
                    else if(wayPoint.getFacilityCode() != null) {
                        rountingLoc.append(","+wayPoint.getFacilityCode());
                    }

                }
            });
        }
        // rountingLoc.deleteCharAt(rountingLoc.length() - 1);
        // unique Routing Country codes
        List<Waypoint> waypointsCountryCodes = handlingUnit.getWaypoints() != null
                ? handlingUnit.getWaypoints().stream().filter(waypoint -> waypoint.getFacilityCountryCode() != null)
                .collect(Collectors.toList())
                : null;
        List<String> countryCodes = waypointsCountryCodes != null
                ? waypointsCountryCodes.stream().map(Waypoint::getFacilityCountryCode).collect(Collectors.toList())
                : null;
        jmsHeaders.put("RoutingCountryCd",
                countryCodes != null ? String.join(",", countryCodes.stream().distinct().collect(Collectors.toList()))
                        : Constants.SINGLE_SPACE);
        jmsHeaders.put("RoutingLoc", rountingLoc.toString());
        jmsHeaders.put("MessageType", "A");
        if (shipmentMessage.getShipmentHeader().getBul010IdOrig() != null) {
            jmsHeaders.put("TrackLocCd", shipmentMessage.getShipmentHeader().getBul010IdOrig());
        }
        jmsHeaders.put("TriggerEventType", "SH");
        jmsHeaders.put("CARRIER_OID", "FDX");

        return jmsHeaders;

    }

    public void setIDMheaders(Message message, Map<String, String> headers) {

        headers.forEach((key, val) -> {
            try {
                message.setStringProperty(key, val);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    private TransactionStatus getTrxStatus() {
        Audit.getInstance().put(AuditKey.ACTION, "Shipment IDM");
        DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
        txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        return transactionManager.getTransaction(txDef);
    }

}
