package com.fedex.uds.streaming.service;


import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.idm.MainShipment;
import com.fedex.uds.common.logging.Audit;
import com.fedex.uds.common.logging.AuditKey;
import com.fedex.uds.common.types.StatusTypes;
import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
import com.fedex.uds.streaming.configuration.UdsPhuwvCollectorProperties;
import com.fedex.uds.streaming.repository.UdsShipmentPublishStatusRepository;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.streaming.service.util.DataProvider;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.util.CompressionUtil;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusId;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class ShipmentIDM {

    @Autowired
    private UdsPhuwvCollectorProperties udsPhuwvCollectorProperties;

    @Autowired
    private UdsShipmentPublishStatusRepository shipemtRepository;

    @Autowired
    private FormIdCheckProperties formIdCheck;

    @Autowired
    private HandlingUnitHandler handler;

    @Autowired
    //@Qualifier("udsShipment")
    private PlatformTransactionManager transactionManager;


    @Autowired
    private UdsShipmentPublishStatusService shipmentPublishService;

    @Autowired
    private ShipmentIDMConversion idmConversion;

    @Autowired
    private DataProvider provider;


    public void shipmentIdmLogic(HandlingUnit handlingUnit, String eventUUId) throws Exception {

        boolean deltaCheckShipment = false;
        MainShipment shipmentDto = perforShipmentIdmConversion(handlingUnit, eventUUId);
        String idmMessage = provider.convertIdmMessage(shipmentDto);

        log.info("IDM_Shipment_Message_New_Single: {} ", idmMessage.replaceAll("[\\n]", ""));

        Serializer serializer = new Persister();
        // Latest Shipment IDM From Database
        String xml = shipmentPublishService
                .getLatestUdsShipmentPublishStatusXmlTopByHandlingUnitUUID(handlingUnit.getHuUUID());
        if (xml == null) {
            updateUdsShipmentStatus(idmMessage, handlingUnit, eventUUId);
            // needs to commit before publish
            handler.publishUDSEvent(idmMessage, Constants.SHIPMENT, handler.getHeaderDetailShipment(shipmentDto, handlingUnit));
            log.info("First PhuwvShipment: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+handler.getHeaderDetailShipment(shipmentDto, handlingUnit)+" "+"MessageBody: "+idmMessage.replaceAll("[\\n]", ""));
            Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
        } else {
            MainShipment shipmentIdm = serializer.read(MainShipment.class,
                    idmMessage.replace("\"\"", "null"));
            MainShipment fromUdsPublishStatus = serializer.read(MainShipment.class, xml);
            // Delta Check For Shipment
            log.info("Delta check begins for Shipment");
            deltaCheckShipment = handler.performDeltaCheckForShipment(shipmentIdm, fromUdsPublishStatus);

            if (deltaCheckShipment) {

                log.info("IDM_Shipment_Delta check: {}", idmMessage.replaceAll("[\\n]", ""));
                updateUdsShipmentStatus(idmMessage, handlingUnit, eventUUId);
                // needs to commit before publish
                handler.publishUDSEvent(idmMessage, Constants.SHIPMENT, handler.getHeaderDetailShipment(shipmentDto, handlingUnit));
                log.info("After Delta PhuwvShipment: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+handler.getHeaderDetailShipment(shipmentDto, handlingUnit)+" "+"MessageBody: "+idmMessage.replaceAll("[\\n]", ""));
                 Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
            } else {
                log.info(" ---No Delta in IDM Shipment Message ---{} ");
            }
        }

    }

    public MainShipment perforShipmentIdmConversion(HandlingUnit handlingUnit, String eventUUUID) throws Exception {
        return idmConversion.perforShipmentIdmConversion(handlingUnit);
    }


    public void updateUdsShipmentStatus(String xmlString, HandlingUnit handlingUnit, String eventUUID) throws Exception {

        UdsShipmentPublishStatusEntity shipmentEntity = new UdsShipmentPublishStatusEntity();
        shipmentEntity.setUdsShipmentPublishStatusId(
                new UdsShipmentPublishStatusId(handlingUnit.getHuUUID(), Instant.now()));
        if (handlingUnit.getHandlingUnitIds() != null && handlingUnit.getHandlingUnitIds().size() > 0) {

            HandlingUnitId handlingUnitId = handlingUnit.getHandlingUnitIds().get(0);

            if (handlingUnitId.getIdType().equalsIgnoreCase(Constants.ENTERPRISE)
                    || handlingUnitId.getIdType().equalsIgnoreCase(Constants.FEDEX)
                    || handlingUnitId.getIdType().equalsIgnoreCase(Constants.EXPRESS)) {

                shipmentEntity.setEnterpriseHandlingUnitId(handlingUnitId.getId());

            } else {
                shipmentEntity.setTntHandlingUnitId(handlingUnitId.getId());
                shipmentEntity.setEnterpriseHandlingUnitId(handlingUnitId.getId());
            }

        } else {
            shipmentEntity.setEnterpriseHandlingUnitId(null);
            shipmentEntity.setTntHandlingUnitId(null);
        }
        List<Waypoint> waypointsCountryCodes = handlingUnit.getWaypoints() != null ? handlingUnit.getWaypoints()
                .stream().filter(waypoint -> waypoint.getFacilityCountryCode() != null).collect(Collectors.toList())
                : null;
        List<String> countryCodes = waypointsCountryCodes != null
                ? waypointsCountryCodes.stream().map(Waypoint::getFacilityCountryCode).collect(Collectors.toList())
                : null;

        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
            if (requirement.getAdditionalAttributes() != null
                    && !(requirement.getAdditionalAttributes().isEmpty())) {
                for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                    List<String> listroutingLocTxt = null;

                    if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("locId")) {
                        listroutingLocTxt = requirement.getAdditionalAttributes().get(i).getValues();
                        shipmentEntity.setRoutingLocationsTxt(String.join(",",
                                listroutingLocTxt.stream().distinct().collect(Collectors.toList())));
                    }

                }

            }

        }
        Serializer serializer = new Persister();
        MainShipment shipmenttIdm = serializer.read(MainShipment.class, xmlString);
        shipmentEntity.setUdsTrkLocCd(shipmenttIdm.getShipmentHeader().getBul010IdOrig());
        log.info("trackLocCd" + shipmenttIdm.getShipmentHeader().getBul010IdOrig());
        if (countryCodes != null) {
            shipmentEntity.setRoutingCountryCdTxt(
                    String.join(",", countryCodes.stream().distinct().collect(Collectors.toList())));
        }
        log.info("countryCodes" + countryCodes);

        shipmentEntity.setPhuwvEventUUID(eventUUID);
        shipmentEntity.setRowPurgeDt(getDefaultPurgeDate());
        shipmentEntity.setPublishedCountNbr((shipemtRepository.count()) + 1);
        convertXmlStringToRaw(xmlString, shipmentEntity);
        shipemtRepository.save(shipmentEntity);
        //  transactionManager.commit(getTrxStatus());
    }


    private void convertXmlStringToRaw(String compactJson, UdsShipmentPublishStatusEntity entity) throws IOException {
        List<byte[]> dataImages = getDataImages(compactJson);
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        int dataImageToSet = dataImages.size();
        entity.setShipmentDataImg1(dataImages.get(0));
        if (dataImages.size() > 1) {
            entity.setShipmentDataImg2(dataImages.get(1));
        }
        if (dataImageToSet > 2) {
            log.error("Data Image Size={}", dataImageToSet);
            log.error("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        }
    }

    private List<byte[]> getDataImages(String compactJson) throws IOException {
        int maxColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumnLength();
        int totalColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumn() * maxColumnLength;
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        log.info("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
        return dataImages;
    }

    private TransactionStatus getTrxStatus() {
        Audit.getInstance().put(AuditKey.ACTION, "Shipment IDM");
        DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
        txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        return transactionManager.getTransaction(txDef);
    }


    public Date getDefaultPurgeDate() {
        Instant now = Instant.now();
        Instant purgeInstant = now.plus(udsPhuwvCollectorProperties.getMaxRetentionDays(), ChronoUnit.DAYS);
        Date date = Date.from(purgeInstant);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

}
