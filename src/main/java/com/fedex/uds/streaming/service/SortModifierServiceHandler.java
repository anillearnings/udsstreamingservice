package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.bre.executorservice.domain.sortmodifier.model.BusinessRuleHandlingCodes;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.WorkRequirement;
import com.fedex.uds.common.dto.idm.MainShipment;
import com.fedex.uds.common.exception.ApplicationException;
import com.fedex.uds.common.exception.ResourceException;
import com.fedex.uds.common.exception.RetryableException;
import com.fedex.uds.common.exception.classifier.ExceptionClassifier;
import com.fedex.uds.common.jms.JMSPublisher;
import com.fedex.uds.common.jms.MessageHandler;
import com.fedex.uds.common.logging.Audit;
import com.fedex.uds.common.logging.AuditKey;
import com.fedex.uds.common.types.StatusTypes;
import com.fedex.uds.streaming.configuration.DeltaCheckProperties;
import com.fedex.uds.streaming.configuration.UdsPhuwvCollectorProperties;
import com.fedex.uds.streaming.repository.UdsInterceptPublishStatusRepository;
import com.fedex.uds.streaming.repository.UdsShipmentPublishStatusRepository;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.util.CompressionUtil;
import com.fedex.uds.util.JsonUtil;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusId;
import com.fedex.uds.view.phuwvevententity.entity.PhuwvEventsEntity;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.ValueChange;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service("SortModifierServiceHandler")
@Slf4j
public class SortModifierServiceHandler implements MessageHandler<BusinessRuleHandlingCodes> {


    @Autowired
    PhuwvEventsService phuwvEventsService;
    boolean deltaCheckIntercept = false;
    private ExceptionClassifier<RetryableException> exceptionClassifier;
    private ObjectMapper objectMapper;
    private RetryTemplate retryTemplate;
    @Autowired
    private UdsInterceptPublishStatusService udsInterceptPublishStatusService;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private JMSPublisher handlingUnitPublisher;
    @Autowired
    private DeltaCheckProperties deltacheck;
    @Autowired
    private UdsInterceptPublishStatusRepository interceptRepository;
    private UdsPhuwvCollectorProperties udsPhuwvCollectorProperties;
    @Autowired
    @Qualifier("SortModifierInterceptIDMConversion")
    private SortModifierInterceptIDMConversion interceptIDMConversion;

    @Autowired
    private UdsShipmentPublishStatusRepository udsRepo;


    @Autowired
    public SortModifierServiceHandler(ExceptionClassifier<RetryableException> exceptionClassifier,
                                      @Qualifier("defaultObjectMapper") ObjectMapper objectMapper,
                                      @Qualifier("jmsRetryTemplate") RetryTemplate retryTemplate,
                                      UdsPhuwvCollectorProperties udsPhuwvCollectorProperties) {

        this.udsPhuwvCollectorProperties = udsPhuwvCollectorProperties;
        this.exceptionClassifier = exceptionClassifier;
        this.objectMapper = objectMapper;
        this.retryTemplate = retryTemplate;
        log.info("SortModifierServiceHandler initialized");
    }

    @Override
    public void handle(GenericMessage<BusinessRuleHandlingCodes> message) throws ResourceException, Exception {

        retryTemplate.execute(retryContext -> {
            Audit.getInstance().put(AuditKey.ACTION, "BusinessRuleHandlingCodes.handle");
            DefaultTransactionDefinition txDef = new DefaultTransactionDefinition();
            txDef.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus transactionStatus = null;
            transactionStatus = transactionManager.getTransaction(txDef);

            try {
                log.info("Received SortModifier Trigger");
                BusinessRuleHandlingCodes businessRuleHandlingCodes = message.getPayload();
                UdsShipmentPublishStatusEntity entity = udsRepo.findTopByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(businessRuleHandlingCodes.getHuUUID());
                log.info("Data from shipment : " + (entity != null ? entity.getPhuwvEventUUID() : "No data"));
                if (entity != null) {
                    String eventUUId = (String) message.getHeaders().get("jms_correlationId");
                    //String eventUUId =null;
                    log.info("BusinessRuleHandlingCodes: {}", businessRuleHandlingCodes);

                    if (log.isDebugEnabled()) {
                        try {
                            String json = JsonUtil.asCompactJson(objectMapper, businessRuleHandlingCodes);
                            log.debug("HandlingUnitPayload: {}", json);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                        }
                    }
                    // Intercept IDM Conversion
                    HandlingUnit handlingUnit = phuwvEventsService.getPhuwvEventsEntityByHandlingUnitUUID(businessRuleHandlingCodes.getHuUUID());

                    // Intercept IDM Conversion
                    List<MainShipment> interceptDtos = interceptIDMConversion.performSortInterceptIdmConversion(businessRuleHandlingCodes, handlingUnit, transactionStatus);
                    if (interceptDtos != null && !interceptDtos.isEmpty()) {
                        performDeltaCheckForIntercept(interceptDtos, businessRuleHandlingCodes, eventUUId, transactionStatus, handlingUnit);
                    }
                    try {
                    transactionManager.commit(transactionStatus);
                    }catch(Exception e) {
                    	log.error(" Error occured while commiting" + businessRuleHandlingCodes.getHuUUID() + e );
                    }
                }
                return null;
            } catch (Throwable e) {
                if (transactionStatus != null && !transactionStatus.isCompleted()) {
                    transactionManager.rollback(transactionStatus);
                }
                throw e;
            }
        }, retryContext -> {
            RetryableException retryableException = null;
            try {
                retryableException = exceptionClassifier.classify(retryContext.getLastThrowable());
            } catch (Throwable e) {
                throw new ApplicationException("Failed to recover: " + ExceptionUtils.getFullStackTrace(e), e);
            }

            if (retryableException != null) {
                String json = null;
                try {
                    // json = JsonUtil.asCompactJson(objectMapper, message.toString());
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
                throw new ResourceException(
                        "Failed to process domain event: " + json + " due to " + retryableException.getMessage(),
                        retryableException);
            }

            return null;
        });
    }

    private String convertIdmMessage(MainShipment shipmentDto) throws Exception {
        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        serializer.write(shipmentDto, writer);
        String idmMessage = writer.toString();
        return idmMessage;
    }

    private boolean performDeltaCheckForYesFields(MainShipment shipmentIdm, MainShipment fromUdsPublishStatus,
                                                  List<String> deltaCheckYesField) {
        boolean deltaChanged = false;
        Javers javers = JaversBuilder.javers().build();
        Diff diff = javers.compare(shipmentIdm, fromUdsPublishStatus);
        List<ValueChange> graph = diff.getChangesByType(ValueChange.class).stream().collect(Collectors.toList());
        for (int i = 0; i < graph.size(); i++) {
            deltaChanged = deltaCheckYesField.contains(graph.get(i).getPropertyNameWithPath().replaceAll("/\\//g.", "")
                    .replaceAll("\\/", ".").replaceAll("\\d\\.", ""));
            if (deltaChanged)
                break;
        }
        return deltaChanged;
    }

    public boolean performDeltaCheckForIntercept(List<MainShipment> interceptDtos,
                                                 BusinessRuleHandlingCodes businessRuleHandlingCodes, String eventUUID,
                                                 TransactionStatus transactionStatus, HandlingUnit handlingUnit) throws IOException {


        List<String> udsInterceptCd = new ArrayList<>();
        List<String> primaryInterceptLocationCd = new ArrayList<>();
        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
            	if (requirement.getType() != null &&(requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)
                        || requirement.getType().equalsIgnoreCase(Constants.GEOGRAPHIC_INTERCEPT)
                        || requirement.getType().equalsIgnoreCase(Constants.FACILITY_INTERCEPT)))
                {
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("clnLocCd")) {
                    String trackLocCd = requirement.getAdditionalAttributes().get(i).getValues().get(0);
                    primaryInterceptLocationCd.add(trackLocCd);
                    break;
                }
            }
            }
        }
        interceptDtos.forEach(listofhu -> {
            if (listofhu.getShipmentHandlingCode() != null && !(listofhu.getShipmentHandlingCode().isEmpty())) {
                udsInterceptCd.add(listofhu.getShipmentHandlingCode().get(0).getHac010Id());
                //primaryInterceptLocationCd.add(listofhu.getShipmentHandlingCode().get(0).getBul010IdOccuring());
            }
        });

        List<MainShipment> fromUdsInterceptPublishStatus = udsInterceptPublishStatusService.getLatestUdsInterceptPublishStatusByHandlingUnitUUIDAndUdsInterceptCodeAndPrimaryInterceptLocationCode(businessRuleHandlingCodes.getHuUUID(), udsInterceptCd, primaryInterceptLocationCd);
        interceptDtos.forEach(interceptDto -> {
            MainShipment fromUdsInterceptPublish = !fromUdsInterceptPublishStatus.isEmpty() ? (fromUdsInterceptPublishStatus.stream()
                    .filter(intercept -> interceptDto.getShipmentHandlingCode().get(0).getHac010Id()
                            .equals(intercept.getShipmentHandlingCode().get(0).getHac010Id()))
                    .findAny().orElse(null)) : null;
            if (fromUdsInterceptPublish == null) {
                // msg with new uds_intercept_cd directly publish and persist
                try {
                    String interceptIdmMessage = convertIdmMessage(interceptDto);
                    log.info("IDM_Intercept Sort Modifier: {} ", interceptIdmMessage.toString().replaceAll("[\\n]", "")); // needs rework
                    log.info("Delta check for Intercept for more triggers ");
                    updateUdsInterceptPublishStatus(interceptIdmMessage, businessRuleHandlingCodes, eventUUID,
                            transactionStatus, handlingUnit);
                    publishUDSEvent(interceptIdmMessage, getHeaderDetailIntercept(interceptIdmMessage));
                    log.info("First SortModifierMessage: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+getHeaderDetailIntercept(interceptIdmMessage)+" "+"MessageBody: "+interceptIdmMessage.replaceAll("[\\n]", ""));

                    Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
                } catch (Exception e) {
                    log.error("" + e);
                }

            } else {
                try {
                    Serializer serializer = new Persister();
                    String interceptIdmMessage = convertIdmMessage(interceptDto);
                    MainShipment interceptIdm = serializer.read(MainShipment.class,
                            interceptIdmMessage.replace("\"\"", "null"));
                    // Delta Check For intercept
                    List<String> deltaCheckYesField = deltacheck.getIntercept().entrySet().stream()
                            .filter(e -> e.getValue().equals('Y')).map(Map.Entry::getKey).collect(Collectors.toList());
                    deltaCheckIntercept = performDeltaCheckForYesFields(interceptIdm, fromUdsInterceptPublish,
                            deltaCheckYesField);

                    if (deltaCheckIntercept) {
                        updateUdsInterceptPublishStatus(interceptIdmMessage, businessRuleHandlingCodes,
                                eventUUID, transactionStatus, handlingUnit);
                        publishUDSEvent(interceptIdmMessage, getHeaderDetailIntercept(interceptIdmMessage));
                        log.info("After Delta SortModifierMessage: "+ handlingUnit.getHuUUID()+" HeaderProperties: "+getHeaderDetailIntercept(interceptIdmMessage)+" "+"MessageBody: "+interceptIdmMessage.replaceAll("[\\n]", ""));
                        Audit.getInstance().put(AuditKey.STATUS, StatusTypes.SUCCESS.name());
                    }
                } catch (Exception e) {
                    log.error("" + e);
                }
            }
        });

        return deltaCheckIntercept;
    }

    public String getJsonFromDataImg(PhuwvEventsEntity entity) throws IOException {
        // Update as data columns are added
        String retString = null;
        byte[] dataImage1 = entity.getDataImg1();
        byte[] dataImage2 = entity.getDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }

        return retString;
    }

    public void updateUdsInterceptPublishStatus(String xmlString, BusinessRuleHandlingCodes businessRuleHandlingCodes, String eventUUID, TransactionStatus transactionStatus, HandlingUnit handlingUnit) throws Exception {


        UdsInterceptPublishStatusEntity interceptEntity = new UdsInterceptPublishStatusEntity();
        Instant updateTimestamp=Instant.now();
        interceptEntity.setUdsInterceptPublishStatusId(
                new UdsInterceptPublishStatusId(businessRuleHandlingCodes.getHuUUID(), updateTimestamp));
        log.info("updateTimestamp for HUUID: "+ updateTimestamp + "BRE HU ID" + businessRuleHandlingCodes.getHuUUID());
        interceptEntity.setPhuwvEventUUID(eventUUID);
        interceptEntity.setRowPurgeDt(getDefaultPurgeDate());
        Serializer serializer = new Persister();
        MainShipment interceptIdm = serializer.read(MainShipment.class, xmlString);
        if (interceptIdm.getShipmentHandlingCode() != null) {
            interceptEntity.setUdsInterceptCode(interceptIdm.getShipmentHandlingCode().get(0).getHac010Id());
            if (interceptIdm.getShipmentHandlingCode().get(0).getGlobalCd().equalsIgnoreCase("Y")) {
                interceptEntity.setMessageSubTypeCode("G");
            } else {
                interceptEntity.setMessageSubTypeCode("L");
            }
        }
        if (handlingUnit.getHandlingUnitIds() != null && handlingUnit.getHandlingUnitIds().size() > 0) {

            HandlingUnitId handlingUnitId = handlingUnit.getHandlingUnitIds().get(0);

            if (handlingUnitId.getIdType().equalsIgnoreCase("ENTERPRISE")
                    || handlingUnitId.getIdType().equalsIgnoreCase("FEDEX")
                    || handlingUnitId.getIdType().equalsIgnoreCase("EXPRESS")) {

                interceptEntity.setEnterpriseHandlingUnitId(handlingUnitId.getId());

            } else {
                interceptEntity.setTntHandlingUnitId(handlingUnitId.getId());
            }

        } else {
            interceptEntity.setEnterpriseHandlingUnitId(null);
            interceptEntity.setTntHandlingUnitId(null);
        }
        List<Waypoint> waypointsCountryCodes = handlingUnit.getWaypoints() != null
                ? handlingUnit.getWaypoints().stream().filter(waypoint -> waypoint.getFacilityCountryCode() != null)
                .collect(Collectors.toList())
                : null;
        List<String> countryCodes = waypointsCountryCodes != null ? waypointsCountryCodes.stream()
                .map(Waypoint::getFacilityCountryCode).collect(Collectors.toList())
                : null;
        interceptEntity.setRoutingCountryCode(countryCodes != null
                ? String.join(",", countryCodes.stream().distinct().collect(Collectors.toList()))
                : null);

        String trackLocCd = null;
        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
        	if (requirement.getType() != null &&(requirement.getType().equalsIgnoreCase(Constants.GLOBAL_INTERCEPT)
                    || requirement.getType().equalsIgnoreCase(Constants.GEOGRAPHIC_INTERCEPT)
                    || requirement.getType().equalsIgnoreCase(Constants.FACILITY_INTERCEPT)))
            {
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("clnLocCd")) {
                    trackLocCd = requirement.getAdditionalAttributes().get(i).getValues().get(0);
                    break;
                }
            }
        }
        	}

        for (WorkRequirement requirement : handlingUnit.getWorkRequirements()) {
            for (int i = 0; i < requirement.getAdditionalAttributes().size(); i++) {
                List<String> listroutingLocTxt = null;
                if (requirement.getAdditionalAttributes().get(i).getKey().equalsIgnoreCase("locId")) {
                    listroutingLocTxt = requirement.getAdditionalAttributes().get(i).getValues();
                    interceptEntity.setRoutingLocationsTxt(
                            String.join(",", listroutingLocTxt.stream().distinct().collect(Collectors.toList())));
                }

            }
        }

        interceptEntity.setPrimaryInterceptLocationCode(trackLocCd);

        // interceptEntity.setPublishedCountNbr((interceptRepository.count()) + 1);
        convertXmlStringToRaw(xmlString, interceptEntity);

        interceptRepository.save(interceptEntity);

        log.info("sort modifier commited");
    }

    private Date getDefaultPurgeDate() {
        Instant now = Instant.now();
        Instant purgeInstant = now.plus(udsPhuwvCollectorProperties.getMaxRetentionDays(), ChronoUnit.DAYS);
        Date date = Date.from(purgeInstant);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private void convertXmlStringToRaw(String compactJson, UdsInterceptPublishStatusEntity entity) throws IOException {
        List<byte[]> dataImages = getDataImages(compactJson);
        entity.setInterceptDataImg1(dataImages.get(0));
        if (dataImages.size() > 1) {
            entity.setInterceptDataImg2(dataImages.get(1));
        }
    }

    private List<byte[]> getDataImages(String compactJson) throws IOException {

        int maxColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumnLength();
        int totalColumnLength = udsPhuwvCollectorProperties.getMaxDataImgColumn() * maxColumnLength;
        byte[] byteBuff = CompressionUtil.gzip(compactJson);
        log.info("COMPACT_JSON_SIZE={}, COMPRESSED_JSON_SIZE={}", compactJson.length(), byteBuff.length);
        List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
        return dataImages;

    }

    public void publishUDSEvent(String xmlContent, Map<String, String> headers) {
        handlingUnitPublisher.publish(xmlContent, new MessagePostProcessor() {
            public Message postProcessMessage(Message message) throws JMSException {

                setIDMheaders(message, headers);
                return message;
            }
        });

    }

    private Map<String, String> getHeaderDetailIntercept(String interceptMessage) throws Exception {
        Serializer serializer = new Persister();

        MainShipment interceptIdm = serializer.read(MainShipment.class, interceptMessage);
        String globalCd = interceptIdm.getShipmentHandlingCode().get(0).getGlobalCd();
        String bul010IdRequested = interceptIdm.getShipmentDetail().getBul010IdRequested();
        String bul010IdOccuring = interceptIdm.getShipmentHandlingCode().get(0).getBul010IdOccuring();
        String opCo = interceptIdm.getShipmentDetail().getSourceSystemCd();
        String barcdType = interceptIdm.getShipmentDetail().getBarCdType();

        Map<String, String> jmsHeaders = new HashMap<>();


        jmsHeaders.put("MsgSource", "UDSAdaptor");
        jmsHeaders.put("GeoCode", interceptIdm.getShipmentHandlingCode().get(0).getBul010IdOccuring());
        jmsHeaders.put("OpCo", opCo.equalsIgnoreCase("FX") ? "FDX" : "TNT");
        jmsHeaders.put("MessageCategoryCd", "IC");
        jmsHeaders.put("FormCd", interceptIdm.getShipmentDetail().getFormId() != null ? interceptIdm.getShipmentDetail().getFormId() : " ");
        jmsHeaders.put("ServiceCd", interceptIdm.getShipmentDetail().getSpeedofService() != null ? interceptIdm.getShipmentDetail().getSpeedofService() : " ");
        jmsHeaders.put("MessageSubTypeCd", globalCd.equalsIgnoreCase("Y") ? "G" : "L");
        jmsHeaders.put("MsgVsn", "1");
        jmsHeaders.put("MsgCharCd", "XML");
        jmsHeaders.put("SpecialHandlingCd", interceptIdm.getShipmentHandlingCode().get(0).getHac010Id());
        jmsHeaders.put("ORIGIN_COUNTRY_CODE", interceptIdm.getShipmentHeader().getCom010CdOrig());
        jmsHeaders.put("OrigLocationCd", interceptIdm.getShipmentHeader().getBul010IdCollection());
        jmsHeaders.put("DEST_COUNTRY_CODE", interceptIdm.getShipmentHeader().getCom010CdDest());
        jmsHeaders.put("DestLocationCd", interceptIdm.getShipmentHeader().getBul010IdDest());
        jmsHeaders.put("RoutingLoc", " ");//need to get clarity
        jmsHeaders.put("MessageType", "A"); // hard coded
        jmsHeaders.put("TriggerEventType", "IC");
        jmsHeaders.put("CARRIER_OID", "FDX"); //hard coded
        if (interceptIdm.getShipmentDetail() != null) {
            if (bul010IdRequested != null) {
                if (bul010IdRequested.equals(bul010IdOccuring)) {
                    jmsHeaders.put("TrackLocCd", bul010IdOccuring);
                }
            } else {
                jmsHeaders.put("TrackLocCd", bul010IdOccuring);

            }
        }

        return jmsHeaders;

    }

    public void setIDMheaders(Message message, Map<String, String> headers) {

        headers.forEach((key, val) -> {
            try {
                message.setStringProperty(key, val);
            } catch (JMSException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
    }

}

