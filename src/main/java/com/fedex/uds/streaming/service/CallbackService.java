package com.fedex.uds.streaming.service;

import com.fedex.uds.common.dto.idm.MainShipment;
import com.fedex.uds.common.dto.idm.SHP160;
import com.fedex.uds.common.exception.ApplicationException;
import com.fedex.uds.common.exception.NoDataFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Service("CallbackService")
@Slf4j
public class CallbackService {

    @Autowired
    UdsShipmentPublishStatusService udsShipmentPublishStatusService;

    @Autowired
    UdsInterceptPublishStatusService udsInterceptPublishStatusService;


    public String getShipmentIdmMessage(String id) {

        String shipmentXml = null;
        List<String> interceptXml = null;
        String idmMessage = null;
        MainShipment mainShipment = new MainShipment();
        ArrayList<SHP160> handlingcodes = null;
        try {
            if (StringUtils.isNotEmpty(id)) {

                if (id.contains("-")) {
                    shipmentXml = udsShipmentPublishStatusService
                            .getLatestUdsShipmentPublishStatusXmlByHandlingUnitUUID(id);

                    if (shipmentXml != null && !shipmentXml.isEmpty())
                        mainShipment = convertToMainShipment(shipmentXml);

                    interceptXml = udsInterceptPublishStatusService
                            .getLatestUdsInterceptPublishStatusXmlByHandlingUnitUUID(id);

                    if (interceptXml != null && !interceptXml.isEmpty())
                        handlingcodes = getHandlingCodeList(interceptXml);

                    mainShipment.setShipmentHandlingCode(handlingcodes);

                    if (shipmentXml != null && !shipmentXml.isEmpty())
                        idmMessage = convertToXml(mainShipment);
                    else {
                        throw new NoDataFoundException(
                                "No Data found in Uds ShipmentPublish Status table with uuid:" + id);
                    }
                    log.info("callback response by handlingUnitID :" + idmMessage.replaceAll("[\\n]", ""));

                } else {

                    shipmentXml = udsShipmentPublishStatusService
                            .getLatestUdsShipmentPublishStatusXmlByEnterpriseHandlingUnitId(id);

                    if (shipmentXml != null && !shipmentXml.isEmpty())
                        mainShipment = convertToMainShipment(shipmentXml);

                    interceptXml = udsInterceptPublishStatusService
                            .getLatestUdsInterceptPublishStatusXmlByEnterpriseHandlingUnitId(id);
                    if (interceptXml != null && !interceptXml.isEmpty())
                        handlingcodes = getHandlingCodeList(interceptXml);

                    mainShipment.setShipmentHandlingCode(handlingcodes);

                    if (shipmentXml != null && !shipmentXml.isEmpty())
                        idmMessage = convertToXml(mainShipment);
                    else {
                        throw new NoDataFoundException(
                                "No Data found in Uds ShipmentPublish Status table with tracking number:"
                                        + id);
                    }
                    log.info("callback response by trackingNumber :" + idmMessage.replaceAll("[\\n]", ""));

                }

            } else {
                throw new ApplicationException("UUID should be not null");
            }


            return idmMessage;
        } catch (Exception e) {

            log.error("Throwable. Failed to get latest xml for Shipment Publish", e);
            throw new ApplicationException(e.getMessage());
        }
    }

    private String convertToXml(MainShipment mainShipment) throws Exception {

        Serializer serializer = new Persister();
        StringWriter writer = new StringWriter();
        serializer.write(mainShipment, writer);
        String idmMessage = writer.toString();

        return idmMessage;

    }

    private MainShipment convertToMainShipment(String shipmentXml) throws Exception {

        Serializer serializer = new Persister();
        log.info("Main Shipment in EndPoint ", shipmentXml);

        MainShipment mainShipment = serializer.read(MainShipment.class, shipmentXml.replace("\"\"", "null"));

        return mainShipment;
    }

    private ArrayList<SHP160> getHandlingCodeList(List<String> interceptXml) throws Exception {

        Serializer serializer = new Persister();
        ArrayList<SHP160> handlingCodesShp160 = new ArrayList<>();
        for (String intercept : interceptXml) {

            MainShipment mainShipment = serializer.read(MainShipment.class, intercept);

            if (mainShipment.getShipmentHandlingCode() != null) {
                ArrayList<SHP160> handlingCode = mainShipment.getShipmentHandlingCode();

                handlingCodesShp160.add(handlingCode.get(0));
            }

        }
        return handlingCodesShp160;
    }


}
