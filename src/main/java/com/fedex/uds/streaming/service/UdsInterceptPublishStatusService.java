package com.fedex.uds.streaming.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fedex.uds.common.dto.UdsInterceptPublishStatusDTO;
import com.fedex.uds.common.dto.idm.MainShipment;
import com.fedex.uds.streaming.repository.UdsInterceptPublishStatusRepository;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class UdsInterceptPublishStatusService {

    @Autowired
    UdsInterceptPublishStatusRepository udsInterceptPublishStatusRepository;

    public UdsInterceptPublishStatusDTO getLatestUdsInterceptPublishStatusByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsInterceptPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        UdsInterceptPublishStatusDTO dto = new UdsInterceptPublishStatusDTO();

        UdsInterceptPublishStatusEntity inteceptpublishstatusentity = udsInterceptPublishStatusRepository
                .findFirstByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);
        if (inteceptpublishstatusentity != null) {
            dto.setPhuwvEventUUID(inteceptpublishstatusentity.getPhuwvEventUUID());
        }
        return dto;
    }

    public List<String> getLatestUdsInterceptPublishStatusXmlByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        List<String> intercepts = new ArrayList<String>();

        List<UdsInterceptPublishStatusEntity> interceptpublishstatusentity = udsInterceptPublishStatusRepository
                .findByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);
        List<UdsInterceptPublishStatusEntity> interceptpublishstatusentityDistinct = getDistinctUdsInterceptPublishStatusEntity(interceptpublishstatusentity);
        if (interceptpublishstatusentity != null)
            for (UdsInterceptPublishStatusEntity xml : interceptpublishstatusentityDistinct) {

                String intercept_xml = getXmlFromDataImg(xml);
                intercepts.add(intercept_xml);
            }

        return intercepts;

    }

    private List<UdsInterceptPublishStatusEntity> getDistinctUdsInterceptPublishStatusEntity(List<UdsInterceptPublishStatusEntity> interceptpublishstatusentity) {
        Map<String, UdsInterceptPublishStatusEntity> hm = new HashMap<>();
        for (int i = (interceptpublishstatusentity.size() - 1); i >= 0; i--) {
            hm.put(interceptpublishstatusentity.get(i).getUdsInterceptCode() + interceptpublishstatusentity.get(i).getMessageSubTypeCode(), interceptpublishstatusentity.get(i));
        }
        List<UdsInterceptPublishStatusEntity> uniqueInterceptpublishstatusentity = new ArrayList<>();

        for (Map.Entry mapElement : hm.entrySet()) {
            uniqueInterceptpublishstatusentity.add((UdsInterceptPublishStatusEntity) mapElement.getValue());
        }
        return uniqueInterceptpublishstatusentity;
    }

    public String getLatestUdsInterceptPublishStatusXmlTopByHandlingUnitUUID(String handlingUnitUUID)
            throws JsonParseException, JsonMappingException, IOException {
        log.info("-DIVE- Calling getLatestUdsShipmentPublishStatusByHandlingUnitUUID handlingUnitUUID: "
                + handlingUnitUUID);

        String phuwEventUUID = null;
        UdsInterceptPublishStatusEntity interceptpublishstatusentity = udsInterceptPublishStatusRepository
                .findTopByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
                        handlingUnitUUID);

        if (interceptpublishstatusentity != null)
            phuwEventUUID = interceptpublishstatusentity.getPhuwvEventUUID();
        return phuwEventUUID;
    }

    /*
     * public List<MainShipment>
     * getLatestUdsInterceptPublishStatusByHandlingUnitUUIDAndPhuwvEventUUID( String
     * handlingUnitUUID, String phuwvEventUUID) throws IOException {
     * List<MainShipment> fromUdsInterceptPublishStatus = new
     * ArrayList<MainShipment>(); Serializer serializer = new Persister();
     * List<UdsInterceptPublishStatusEntity> interceptpublishstatusentities =
     * udsInterceptPublishStatusRepository
     * .findByHandlingUnitUUIDAndPhuwvEventUUID(handlingUnitUUID, phuwvEventUUID);
     *
     * if (interceptpublishstatusentities != null &&
     * !(interceptpublishstatusentities.isEmpty())) {
     * interceptpublishstatusentities.forEach(interceptpublishstatusentity -> { try
     * { String xml = getXmlFromDataImg(interceptpublishstatusentity); MainShipment
     * fromUdsPublishStatus = serializer.read(MainShipment.class, xml);
     * fromUdsInterceptPublishStatus.add(fromUdsPublishStatus); } catch (Exception
     * e) {
     *
     * } }); } return fromUdsInterceptPublishStatus;
     *
     * }
     */

    public List<MainShipment> getLatestUdsInterceptPublishStatusByHandlingUnitUUIDAndUdsInterceptCodeAndPrimaryInterceptLocationCode(
            String handlingUnitUUID, Collection<String> udsInterceptCode, Collection<String> primaryInterceptLocationCode) throws IOException {
        List<MainShipment> fromUdsInterceptPublishStatus = new ArrayList<MainShipment>();
        Serializer serializer = new Persister();
        List<UdsInterceptPublishStatusEntity> interceptpublishstatusentities = udsInterceptPublishStatusRepository
                .findByHandlingUnitUUIDAndUdsInterceptCodeAndPrimaryInterceptLocationCode(handlingUnitUUID, udsInterceptCode, primaryInterceptLocationCode);

        if (interceptpublishstatusentities != null && !(interceptpublishstatusentities.isEmpty())) {
            interceptpublishstatusentities.forEach(interceptpublishstatusentity -> {
                try {
                    String xml = getXmlFromDataImg(interceptpublishstatusentity);
                    MainShipment fromUdsPublishStatus = serializer.read(MainShipment.class, xml);
                    fromUdsInterceptPublishStatus.add(fromUdsPublishStatus);
                } catch (Exception e) {

                }
            });
        }
        return fromUdsInterceptPublishStatus;

    }

    public String getXmlFromDataImg(UdsInterceptPublishStatusEntity entity) throws IOException {
        // Update as data columns are added
        String retString = null;
        byte[] dataImage1 = entity.getInterceptDataImg1();
        byte[] dataImage2 = entity.getInterceptDataImg2();
        if ((null != dataImage1) || (null != dataImage2)) {
            return BytesUtil.convertDataImageToJson(dataImage1, dataImage2);
        }

        return retString;
    }

    public List<String> getLatestUdsInterceptPublishStatusXmlByEnterpriseHandlingUnitId(String trknbr) throws IOException {

        log.info("-DIVE- Calling getLatestUdsInterceptPublishStatusXmlByEnterpriseHandlingUnitId trackingNumber: "
                + trknbr);

        List<String> intercepts = new ArrayList<String>();

        List<UdsInterceptPublishStatusEntity> interceptpublishstatusentity = udsInterceptPublishStatusRepository
                .findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(trknbr);
        List<UdsInterceptPublishStatusEntity> interceptpublishstatusentityDistinct = getDistinctUdsInterceptPublishStatusEntity(interceptpublishstatusentity);
        if (interceptpublishstatusentity != null)
            for (UdsInterceptPublishStatusEntity xml : interceptpublishstatusentityDistinct) {

                String intercept_xml = getXmlFromDataImg(xml);
                intercepts.add(intercept_xml);
            }

        return intercepts;
    }

}
