package com.fedex.uds.streaming.service;

import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.common.exception.NoDataFoundException;
import com.fedex.uds.streaming.client.TranslationMappingServiceClient;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TranslationMappingServiceInvoker")
@Slf4j
public class TranslationMappingServiceInvoker {

    @Autowired
    TranslationMappingServiceClient translationMappingServiceClient;

    public List<UdsDGMappingEntity> getUdsDGMappingEntities(List<UdsDGMappingEntity> udsDGMappingEntities) {
        ResponseEntity<List<UdsDGMappingEntity>> udsDGMappingResponse = translationMappingServiceClient.getDgCodes(udsDGMappingEntities);
        if (udsDGMappingResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            log.warn("DGMapping not found");
            throw new NoDataFoundException("DGMapping not found");
        }
        return udsDGMappingResponse.getBody();
    }

    public List<UdsServiceCodeMappingEntity> getUdsServiceCodeMappingEntities(List<UdsServiceCodeMappingEntity> udsServiceCodeMappingEntities) {
        ResponseEntity<List<UdsServiceCodeMappingEntity>> udsServiceCodeMappingResponse = translationMappingServiceClient.getServiceCodes(udsServiceCodeMappingEntities);
        if (udsServiceCodeMappingResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            log.warn("ServiceCodeMapping not found");
            throw new NoDataFoundException("ServiceCodeMapping not found");
        }
        return udsServiceCodeMappingResponse.getBody();
    }

    public List<UdsHandlingCodeMappingEntity> getudsHandlingCodeMappingEntities(List<UdsHandlingCodeMappingEntity> udsHandlingCodeMappingEntities) {

        ResponseEntity<List<UdsHandlingCodeMappingEntity>> udsHandlingCodeMappingResponse = translationMappingServiceClient.getHandlingCodes(udsHandlingCodeMappingEntities);
        if (udsHandlingCodeMappingResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            log.warn("HandlingCodeMapping not found");
            throw new NoDataFoundException("HandlingCodeMapping not found");
        }
        return udsHandlingCodeMappingResponse.getBody();
    }

    public MappingCodesDTO getTranslationsResult(MappingCodesDTO mappingCodeDTO) {
        ResponseEntity<MappingCodesDTO> udsTranslationResponse = translationMappingServiceClient.getTranslations(mappingCodeDTO);
        if (udsTranslationResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            log.warn("Translation result not found for given mappingCodeDTO:" + mappingCodeDTO);
            throw new NoDataFoundException("Translation result not found");
        }
        return udsTranslationResponse.getBody();
    }

    public MappingCodesDTO getAllMappingCodes() {
        ResponseEntity<MappingCodesDTO> allMappingResponse = translationMappingServiceClient.getAllMappingCodes();
        if (allMappingResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            log.warn("Failed to getAllMappingCodes");
            throw new NoDataFoundException("Translation result not found");
        }
        return allMappingResponse.getBody();

    }

}
