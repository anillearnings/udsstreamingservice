package com.fedex.uds.streaming.service;

import com.fedex.bre.executorservice.domain.sortmodifier.model.BusinessRuleHandlingCodes;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.uds.common.dto.MappingCodesDTO;
import com.fedex.uds.common.dto.idm.*;
import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
import com.fedex.uds.streaming.service.util.Constants;
import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("SortModifierInterceptIDMConversion")
@Slf4j
public class SortModifierInterceptIDMConversion {
    @Autowired
    @Qualifier("TranslationMappingServiceInvoker")
    private TranslationMappingServiceInvoker mappingServiceInvoker;

    @Autowired
    private FormIdCheckProperties formIdCheck;

    public List<MainShipment> performSortInterceptIdmConversion(BusinessRuleHandlingCodes businessRuleHandlingCodes,
	                                                            HandlingUnit handlingUnit, TransactionStatus transactionStatus) throws Exception {
	
	    List<MainShipment> listofshipments = new ArrayList<MainShipment>();
	
	    if (handlingUnit != null) {
	        if (businessRuleHandlingCodes != null) {
	            if (businessRuleHandlingCodes.getLocationHandlingCodes() != null
	                    && !(businessRuleHandlingCodes.getLocationHandlingCodes().isEmpty())) {
	                businessRuleHandlingCodes.getLocationHandlingCodes().forEach(locationcodes -> {
	                    if (locationcodes.getLocationCode() != null && !(locationcodes.getLocationCode().isEmpty())) {
	                        if (locationcodes.getLocationCode().equalsIgnoreCase("global")) {
	                            if (locationcodes.getHandlingCodes() != null
	                                    && !(locationcodes.getHandlingCodes().isEmpty())) {
	
	                                List<UdsHandlingCodeMappingEntity> handlingCodeMappingEntityList = new ArrayList<>();
	                                locationcodes.getHandlingCodes().forEach(locationhandlingcode -> {
	                                    UdsHandlingCodeMappingEntity udsHandlingEntity = new UdsHandlingCodeMappingEntity();
	
	                                    udsHandlingEntity.setHandlingInterceptCd(locationhandlingcode.getHandlingCode()
	                                            .substring(1, locationhandlingcode.getHandlingCode().length() - 1));
	                                    handlingCodeMappingEntityList.add(udsHandlingEntity);
	
	                                    /*
	                                     * if(handlingUnit.getLegacyServiceCode()!=null) {
	                                     * serviceEntity.setServiceCd(handlingUnit.getLegacyServiceCode() != null ?
	                                     * handlingUnit.getLegacyServiceCode() : null); }
	                                     */
	
	                                });
	                                MappingCodesDTO translationsResult = mappingServiceInvoker
	                                        .getTranslationsResult(getMappingCodes(handlingCodeMappingEntityList));
	
	                                log.info("translationsResult : " + translationsResult);
	
	                                try {
	                                    if (translationsResult != null
	                                            && translationsResult.getUdsHandlingCodeMappingEntities() != null
	                                            && !translationsResult.getUdsHandlingCodeMappingEntities().isEmpty()) {
	                                        List<String> interceptCd = new ArrayList<>();
	                                        translationsResult.getUdsHandlingCodeMappingEntities().stream().distinct()
	                                                .collect(Collectors.toList()).forEach(handlingcodesentity -> {
	                                                    if (handlingcodesentity != null
	                                                            && handlingcodesentity.getSorterInterceptCd() != null) {
	                                                        MainShipment shipment = new MainShipment();
	                                                        SHP010 shipmentHeader = new SHP010();
	                                                        SHP170 shipmentDetail = new SHP170();
	                                                        SHP160 shipmentHandlingCode = new SHP160();
	                                                        ArrayList<SHP160> shipmentHandlingCodes = new ArrayList<SHP160>();
	                                                        if (handlingUnit.getHandlingUnitIds() != null
	                                                                && !(handlingUnit.getHandlingUnitIds().isEmpty())) {
	
	                                                            for (HandlingUnitId h : handlingUnit
	                                                                    .getHandlingUnitIds()) {
	                                                                if (h.getIdType() != null) {
	                                                                    if (h.getIdType().equals(Constants.ENTERPRISE)
	                                                                            || h.getIdType().equals(Constants.FEDEX)
	                                                                            || h.getIdType()
	                                                                            .equals(Constants.EXPRESS)) {
	                                                                        shipmentHeader
	                                                                                .setCustomerReferenceNr(h.getId());
	                                                                    }
	                                                                    if (h.getIdType()
	                                                                            .equals(Constants.FRANCE_DOMESTIC)) {
	                                                                        shipmentDetail
	                                                                                .setDomesticShipmentNr(h.getId());
	                                                                        shipmentDetail.setDomesticShipmentType(
	                                                                                h.getIdType());
	                                                                        shipmentHeader.setId(h.getId());
	                                                                    }
	                                                                    if (h.getIdType().equals(Constants.TNT)) {
	                                                                        shipmentHeader.setId(h.getId());
	                                                                    }
	                                                                }
	                                                            }
	                                                        }
	                                                        if (handlingUnit.getShipment() != null) {
	                                                            shipmentDetail.setMasterShipmentId(handlingUnit
	                                                                    .getShipment().getMasterTrackingNumber());
	                                                        }
	                                                        List<String> orangeIds = formIdCheck.getFormId();
	                                                        Boolean orangePackage = orangeIds
	                                                                .contains(handlingUnit.getFormID());
	                                                        if (orangePackage) {
	                                                            shipmentDetail.setSourceSystemCd("TNT");
	                                                        } else {
	                                                            shipmentDetail.setSourceSystemCd("FX");
	                                                        }
	                                                        if (handlingUnit.getWaypoints() != null
	                                                                && !(handlingUnit.getWaypoints().isEmpty())) {
	                                                            handlingUnit.getWaypoints().forEach(waypoint -> {
	                                                                if (waypoint.getType() != null) {
	                                                                    if (waypoint.getType().equalsIgnoreCase(
	                                                                            Constants.DESTINATION)&& waypoint.getFacilityCode()!=null) {
	                                                                        shipmentHandlingCode.setBul010IdOccuring(
	                                                                                waypoint.getFacilityCode());
	                                                                        shipmentHeader.setBul010IdDest(
	                                                                                waypoint.getFacilityCode());
	                                                                    }
	                                                                    if (waypoint.getType()
	                                                                            .equalsIgnoreCase(Constants.ORIGIN)&& waypoint.getFacilityCode()!=null) {
	                                                                        shipmentHeader.setBul010IdCollection(
	                                                                                waypoint.getFacilityCode());
	                                                                        shipmentHeader.setBul010IdOrig(
	                                                                                waypoint.getFacilityCode());
	                                                                    }
	                                                                }
	                                                            });
	
	                                                        }
	
	
	                                                        shipmentHandlingCode
	                                                                .setHandlingCodeReleaseInd("N");
	                                                        shipmentHandlingCode.setGlobalCd("Y");
	
	                                                        if (handlingUnit.getShipment() != null) {
	                                                            if (handlingUnit.getShipment()
	                                                                    .getCustomerAssociations() != null
	                                                                    && !(handlingUnit.getShipment()
	                                                                    .getCustomerAssociations().isEmpty())) {
	                                                                handlingUnit.getShipment().getCustomerAssociations()
	                                                                        .forEach(associations -> {
	                                                                            if (associations
	                                                                                    .getCustomerType() != null) {
	                                                                                if (associations.getCustomerType()
	                                                                                        .equalsIgnoreCase(
	                                                                                                Constants.RECIPIENT)) {
	                                                                                    shipmentHeader.setCom010CdDest(
	                                                                                            handlingUnit.getDestinationCountryCode()!=null? handlingUnit.getDestinationCountryCode():" ");
	
	                                                                                } else if (associations
	                                                                                        .getCustomerType()
	                                                                                        .equalsIgnoreCase(
	                                                                                                Constants.SHIPPER)) {
	                                                                                    shipmentHeader.setCom010CdOrig(
	                                                                                            handlingUnit
	                                                                                                    .getOriginCountryCode()!=null? handlingUnit.getDestinationCountryCode():" ");
	                                                                                }
	                                                                            }
	                                                                        });
	
	                                                            }
	                                                        }
	                                                        //shipmentHeader.setBul010IdDest("");
	                                                        ArrayList<SHP880> shipmentRouteList = new ArrayList<SHP880>();
	                                                        for (int i = 0; i < handlingUnit.getWaypoints()
	                                                                .size(); i++) {
	
	                                                            SHP880 shipmentRoute = new SHP880();
	
	                                                            Waypoint waypoint = handlingUnit.getWaypoints().get(i);
	                                                            if ((i + 1) == handlingUnit.getWaypoints().size()) {
	                                                                break;
	                                                            } else {
	                                                                if (waypoint.getSequenceNumber() != null) {
	                                                                    if (shipmentHeader
	                                                                            .getCustomerReferenceNr() != null) {
	                                                                        shipmentRoute.setId(shipmentHeader
	                                                                                .getCustomerReferenceNr() + "_"
	                                                                                + waypoint.getSequenceNumber()
	                                                                                .toString());
	                                                                    }
	                                                                    shipmentRoute.setSequencingNr(waypoint
	                                                                            .getSequenceNumber().toString());
	                                                                }
	                                                                shipmentRoute.setBul010IdRouting(
	                                                                        waypoint.getFacilityCode());
	                                                                shipmentRoute
	                                                                        .setBul010IdNext(handlingUnit.getWaypoints()
	                                                                                .get(i + 1).getFacilityCode());
	
	                                                            }
	                                                            shipmentRouteList.add(shipmentRoute);
	                                                        }
	                                                        shipment.setShipmentRoute(shipmentRouteList);
	                                                        interceptCd.add(handlingcodesentity.getSorterInterceptCd());
	                                                        shipmentHandlingCode.setHac010Id(
	                                                                handlingcodesentity.getSorterInterceptCd());
	                                                        shipmentHandlingCodes.add(shipmentHandlingCode);
	                                                        shipment.setShipmentHandlingCode(shipmentHandlingCodes);
	                                                        shipment.setShipmentHeader(shipmentHeader);
	                                                        shipment.setShipmentDetail(shipmentDetail);
	                                                        listofshipments.add(shipment);
	                                                    }
	                                                });
	                                    }
	
	                                } catch (Exception e) {
	                                    log.info("Exception sort" + e.getMessage());
	
	                                }
	
	                            }
	
	                        } else {
	
	
	                            if (locationcodes.getHandlingCodes() != null
	                                    && !(locationcodes.getHandlingCodes().isEmpty())) {
	
	                                List<UdsHandlingCodeMappingEntity> handlingCodeMappingEntityList = new ArrayList<>();
	                                locationcodes.getHandlingCodes().forEach(locationhandlingcode -> {
	                                    UdsHandlingCodeMappingEntity udsHandlingEntity = new UdsHandlingCodeMappingEntity();
	
	                                    udsHandlingEntity.setHandlingInterceptCd(locationhandlingcode.getHandlingCode()
	                                            .substring(1, locationhandlingcode.getHandlingCode().length() - 1));
	                                    handlingCodeMappingEntityList.add(udsHandlingEntity);
	
	                                    /*
	                                     * if(handlingUnit.getLegacyServiceCode()!=null) {
	                                     * serviceEntity.setServiceCd(handlingUnit.getLegacyServiceCode() != null ?
	                                     * handlingUnit.getLegacyServiceCode() : null); }
	                                     */
	
	                                });
	                                MappingCodesDTO translationsResult = mappingServiceInvoker
	                                        .getTranslationsResult(getMappingCodes(handlingCodeMappingEntityList));
	
	                                log.info("translationsResult : " + translationsResult);
	
	                                try {
	                                    if (translationsResult != null
	                                            && translationsResult.getUdsHandlingCodeMappingEntities() != null
	                                            && !translationsResult.getUdsHandlingCodeMappingEntities().isEmpty()) {
	                                        List<String> interceptCd = new ArrayList<>();
	                                        translationsResult.getUdsHandlingCodeMappingEntities().stream().distinct()
	                                                .collect(Collectors.toList()).forEach(handlingcodesentity -> {
	                                                    if (handlingcodesentity != null
	                                                            && handlingcodesentity.getSorterInterceptCd() != null) {
	                                                        MainShipment shipment = new MainShipment();
	                                                        SHP010 shipmentHeader = new SHP010();
	                                                        SHP170 shipmentDetail = new SHP170();
	                                                        SHP160 shipmentHandlingCode = new SHP160();
	                                                        ArrayList<SHP160> shipmentHandlingCodes = new ArrayList<SHP160>();
	                                                        if (handlingUnit.getHandlingUnitIds() != null
	                                                                && !(handlingUnit.getHandlingUnitIds().isEmpty())) {
	
	                                                            for (HandlingUnitId h : handlingUnit
	                                                                    .getHandlingUnitIds()) {
	                                                                if (h.getIdType() != null) {
	                                                                    if (h.getIdType().equals(Constants.ENTERPRISE)
	                                                                            || h.getIdType().equals(Constants.FEDEX)
	                                                                            || h.getIdType()
	                                                                            .equals(Constants.EXPRESS)) {
	                                                                        shipmentHeader
	                                                                                .setCustomerReferenceNr(h.getId());
	                                                                    }
	                                                                    if (h.getIdType()
	                                                                            .equals(Constants.FRANCE_DOMESTIC)) {
	                                                                        shipmentDetail
	                                                                                .setDomesticShipmentNr(h.getId());
	                                                                        shipmentDetail.setDomesticShipmentType(
	                                                                                h.getIdType());
	                                                                        shipmentHeader.setId(h.getId());
	                                                                    }
	                                                                    if (h.getIdType().equals(Constants.TNT)) {
	                                                                        shipmentHeader.setId(h.getId());
	                                                                    }
	                                                                }
	                                                            }
	                                                        }
	                                                        if (handlingUnit.getShipment() != null) {
	                                                            shipmentDetail.setMasterShipmentId(handlingUnit
	                                                                    .getShipment().getMasterTrackingNumber());
	                                                        }
	                                                        List<String> orangeIds = formIdCheck.getFormId();
	                                                        Boolean orangePackage = orangeIds
	                                                                .contains(handlingUnit.getFormID());
	                                                        if (orangePackage) {
	                                                            shipmentDetail.setSourceSystemCd("TNT");
	                                                        } else {
	                                                            shipmentDetail.setSourceSystemCd("FX");
	                                                        }
	                                                        shipmentHandlingCode.setBul010IdOccuring(locationcodes.getLocationCode()
	                                                        );
	
	                                                        if (handlingUnit.getWaypoints() != null
	                                                                && !(handlingUnit.getWaypoints().isEmpty())) {
	                                                            handlingUnit.getWaypoints().forEach(waypoint -> {
	                                                                if (waypoint.getType() != null) {
	                                                                    if (waypoint.getType().equalsIgnoreCase(
	                                                                            Constants.DESTINATION)&& waypoint.getFacilityCode()!=null ) {
	
	                                                                        shipmentHeader.setBul010IdDest(
	                                                                                waypoint.getFacilityCode());
	                                                                    }
	                                                                    if (waypoint.getType()
	                                                                            .equalsIgnoreCase(Constants.ORIGIN)&&waypoint.getFacilityCode()!=null) {
	                                                                        shipmentHeader.setBul010IdCollection(
	                                                                                waypoint.getFacilityCode());
	                                                                        shipmentHeader.setBul010IdOrig(
	                                                                                waypoint.getFacilityCode());
	                                                                    }
	                                                                }
	                                                            });
	
	                                                        }
	
	                                                        shipmentHandlingCode
	                                                                .setHandlingCodeReleaseInd("N");
	                                                        shipmentHandlingCode.setGlobalCd("N");
	
	                                                        if (handlingUnit.getShipment() != null) {
	                                                            if (handlingUnit.getShipment()
	                                                                    .getCustomerAssociations() != null
	                                                                    && !(handlingUnit.getShipment()
	                                                                    .getCustomerAssociations().isEmpty())) {
	                                                                handlingUnit.getShipment().getCustomerAssociations()
	                                                                        .forEach(associations -> {
	                                                                            if (associations
	                                                                                    .getCustomerType() != null) {
	                                                                                if (associations.getCustomerType()
	                                                                                        .equalsIgnoreCase(
	                                                                                                Constants.RECIPIENT)) {
	                                                                                    shipmentHeader.setCom010CdDest(
	                                                                                            handlingUnit
	                                                                                                    .getDestinationCountryCode()!=null? handlingUnit.getDestinationCountryCode():" ");
	
	                                                                                } else if (associations
	                                                                                        .getCustomerType()
	                                                                                        .equalsIgnoreCase(
	                                                                                                Constants.SHIPPER)) {
	                                                                                    shipmentHeader.setCom010CdOrig(
	                                                                                            handlingUnit
	                                                                                                    .getOriginCountryCode()!=null? handlingUnit.getDestinationCountryCode():" ");
	                                                                                }
	                                                                            }
	                                                                        });
	
	                                                            }
	                                                        }
	
	
	                                                        interceptCd.add(handlingcodesentity.getSorterInterceptCd());
	                                                        shipmentHandlingCode.setHac010Id(
	                                                                handlingcodesentity.getSorterInterceptCd());
	                                                        shipmentHandlingCodes.add(shipmentHandlingCode);
	                                                        shipment.setShipmentHandlingCode(shipmentHandlingCodes);
	                                                        shipment.setShipmentHeader(shipmentHeader);
	                                                        shipment.setShipmentDetail(shipmentDetail);
	                                                        listofshipments.add(shipment);
	                                                    }
	                                                });
	                                    }
	
	                                } catch (Exception e) {
	                                    log.info("Exception sort" + e.getMessage());
	
	                                }
	
	                            }
	
	
	                        }
	                    }
	
	                });
	            }
	        }
	    }
	    return listofshipments;
	}

    private MappingCodesDTO getMappingCodes(List<UdsHandlingCodeMappingEntity> entities) {
        MappingCodesDTO mappingCodes = new MappingCodesDTO();
        mappingCodes.setUdsHandlingCodeMappingEntities(entities);
        List<UdsDGMappingEntity> dgEntities = new ArrayList<>();
        List<UdsServiceCodeMappingEntity> serviceEntities = new ArrayList<>();
        mappingCodes.setUdsDGMappingEntities(dgEntities);
        mappingCodes.setUdsServiceCodeMappingEntities(serviceEntities);
        log.info("MappingDto Info" + mappingCodes);
        return mappingCodes;
    }

}