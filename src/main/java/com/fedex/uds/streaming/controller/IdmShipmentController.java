package com.fedex.uds.streaming.controller;

import com.fedex.uds.streaming.service.CallbackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/uds")
public class IdmShipmentController {

    @Autowired
    private CallbackService callbackService;

    @GetMapping("/shipmentPublishStatus")
    public ResponseEntity<String> getShipmentIdmMessage(@RequestParam("handlingUnitUUID") String handlingUnitUUID) {

        String mainShipment = callbackService.getShipmentIdmMessage(handlingUnitUUID);

        log.info("Callback response : " + mainShipment);

        return new ResponseEntity<>(mainShipment, HttpStatus.OK);

    }

}
