package com.fedex.uds.streaming.controller;


import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.uds.common.dto.ConsRetreivalDTO;
import com.fedex.uds.streaming.service.ConsRetrievalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/uds")
public class ConsRetreivalController {

    @Autowired
    @Qualifier("ConsRetrievalService")
    private ConsRetrievalService consRetrievalService;

    @GetMapping("/retrieveConsByTrackingNbr")
    public ResponseEntity<ConsRetreivalDTO> getConsByTrackingNbr(@RequestParam("trknbr") String trknbr) throws Exception {

        ConsRetreivalDTO handlingCodes = consRetrievalService.getLatestConsByTrknbr(trknbr);
        return new ResponseEntity<>(handlingCodes, HttpStatus.OK);
    }

    @PostMapping("/retrieveConsByPHUWV")
    public ResponseEntity<ConsRetreivalDTO> getConsInterceptByPhuwvJson(@RequestBody HandlingUnit phuwvJson) {
        return new ResponseEntity<>(consRetrievalService.getLatestConsByPhuvwJson(phuwvJson), HttpStatus.OK);
    }

}