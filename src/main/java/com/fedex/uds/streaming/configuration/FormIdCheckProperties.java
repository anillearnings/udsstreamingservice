package com.fedex.uds.streaming.configuration;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "formids")
@Slf4j
@Data
@NoArgsConstructor
public class FormIdCheckProperties {

    List<String> formId = new ArrayList<>();

    @PostConstruct
    public void initialize() {
        log.info("Properties initialized");
    }

}
