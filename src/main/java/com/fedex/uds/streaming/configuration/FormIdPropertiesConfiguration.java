package com.fedex.uds.streaming.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

@Configuration
@EnableConfigurationProperties(FormIdCheckProperties.class)
@EnableRetry
public class FormIdPropertiesConfiguration {

}
