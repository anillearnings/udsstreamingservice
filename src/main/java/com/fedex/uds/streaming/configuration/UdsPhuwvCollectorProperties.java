package com.fedex.uds.streaming.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@ConfigurationProperties(prefix = "uds.view")
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class UdsPhuwvCollectorProperties {

    private int maxDataImgColumnLength;
    private int maxDataImgColumn;
    private int maxRetentionDays;
    private int nbrDaysAfterHandlingUnitDtToPurge;
    private boolean enableCalcPurgeDate;

    @PostConstruct
    public void initialize() {
        log.info("Properties initialized");
    }

}
