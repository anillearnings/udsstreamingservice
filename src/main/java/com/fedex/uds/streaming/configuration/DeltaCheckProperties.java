package com.fedex.uds.streaming.configuration;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@ConfigurationProperties(prefix = "delta")
@Slf4j
@Data
@NoArgsConstructor
public class DeltaCheckProperties {
    Map<String, Character> shipment = new HashMap<>();
    Map<String, Character> intercept = new HashMap<>();

    @PostConstruct
    public void initialize() {
        log.info("Properties initialized");
    }

}
