package com.fedex.uds.streaming.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.uds.common.dto.ConsRetreivalDTO;
import com.fedex.uds.common.http.rest.GlobalRestExceptionHandler;
import com.fedex.uds.streaming.service.ConsRetrievalService;
import com.fedex.uds.util.JsonUtil;

@RunWith(SpringRunner.class)
public class ConsRetreivalControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private ConsRetreivalController consRetreivalController;

	@Mock
	private ConsRetrievalService service;
	
	
	private ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setUp() throws JsonParseException, JsonMappingException, IOException {
		mockMvc = MockMvcBuilders.standaloneSetup(consRetreivalController)
				.setControllerAdvice(new GlobalRestExceptionHandler()).build();
		
	}

	@Test
	public void givenTrackingNumber_whenGetConsByTrackingNbr_thenReturn200StatusAndConsRetreivalDto() throws Exception {
		Mockito.when(service.getLatestConsByTrknbr(Mockito.anyString())).thenReturn(new ConsRetreivalDTO());
		mockMvc.perform(get("/api/v1/uds/retrieveConsByTrackingNbr?trknbr=457326561714").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void givenPhuwvJsonWhenGetConsInterceptByPhuwvJson_thenReturn200StatusAndConsRetreivalDto() throws Exception {
		HandlingUnit handlingUnit = JsonUtil.json2Obj(objectMapper, "data", "phuwv_json.json", HandlingUnit.class);
		
		Mockito.when(service.getLatestConsByPhuvwJson(Mockito.any(HandlingUnit.class))).thenReturn(new ConsRetreivalDTO());
		mockMvc.perform(post("/api/v1/uds/retrieveConsByPHUWV").contentType(MediaType.APPLICATION_JSON_VALUE).content(toJson(handlingUnit)))
				.andExpect(status().isOk());
	}	

	 public static String toJson(final Object obj) {
	        try {
	            return new ObjectMapper().writeValueAsString(obj);
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	    }
	
}
