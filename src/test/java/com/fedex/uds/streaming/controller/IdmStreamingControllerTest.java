package com.fedex.uds.streaming.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.uds.common.dto.ConsRetreivalDTO;
import com.fedex.uds.common.http.rest.GlobalRestExceptionHandler;
import com.fedex.uds.streaming.service.CallbackService;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class IdmStreamingControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private IdmShipmentController idmShipmentController;

	@Mock
	private CallbackService service;

	@Before
	public void setUp() throws JsonParseException, JsonMappingException, IOException {
		mockMvc = MockMvcBuilders.standaloneSetup(idmShipmentController)
				.setControllerAdvice(new GlobalRestExceptionHandler()).build();

	}

	@Test
	public void givenTrackingNumberWhenGetConsByTrackingNbr_thenReturn200StatusAndConsRetreivalDto() throws Exception {
		Mockito.when(service.getShipmentIdmMessage(Mockito.anyString())).thenReturn(any(String.class));
		mockMvc.perform(get("/api/v1/uds/shipmentPublishStatus?handlingUnitUUID=0019dd27-7785-3883-8146-c9ee136bfce7")
			.accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}

}
