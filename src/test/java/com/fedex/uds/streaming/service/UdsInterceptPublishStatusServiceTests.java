// package com.fedex.uds.streaming.service;

// import java.io.IOException;
// import java.time.Instant;
// import java.util.Date;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.mockito.InjectMocks;
// import org.mockito.MockitoAnnotations;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.boot.test.mock.mockito.MockBean;

// import com.fasterxml.jackson.core.JsonParseException;
// import com.fasterxml.jackson.databind.JsonMappingException;
// import com.fedex.uds.streaming.dto.UdsInterceptPublishStatusDTO;
// import com.fedex.uds.streaming.repository.UdsInterceptPublishStatusRepository;
// import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
// import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusId;

// import static org.assertj.core.api.Assertions.assertThat;
// import static org.mockito.Mockito.when;

// @SpringBootTest
// public class UdsInterceptPublishStatusServiceTests {

// 	@MockBean
// 	UdsInterceptPublishStatusRepository repository;

// 	UdsInterceptPublishStatusEntity udsInterceptEntity = new UdsInterceptPublishStatusEntity();

// 	@Autowired
// 	@InjectMocks
// 	UdsInterceptPublishStatusService service;

// 	@BeforeEach
// 	public void init() {

// 		MockitoAnnotations.initMocks(this);

// 		UdsInterceptPublishStatusId id = new UdsInterceptPublishStatusId("handlingUnitUUID", Instant.now());

// 		udsInterceptEntity.setUdsInterceptPublishStatusId(id);
// 		udsInterceptEntity.setPhuwvEventUUID("phuwvEventUUID");
// 		udsInterceptEntity.setTntHandlingUnitId("tntHandlingUnitId");
// 		udsInterceptEntity.setEnterpriseHandlingUnitId("enterpriseHandlingUnitId");
// 	//	udsInterceptEntity.setPublishedCountNbr(1);
// 		udsInterceptEntity.setRowPurgeDt(new Date());

// 	}

// 	@Test
// 	public void test_GetLatestUdsInterceptPublishStatusByHandlingUnitUUID()
// 			throws JsonParseException, JsonMappingException, IOException {

// 		when(repository
// 				.findFirstByUdsInterceptPublishStatusId_handlingUnitUUIDOrderByUdsInterceptPublishStatusId_rowUpdateTmstpDesc(
// 						"handlingUnitUUID0")).thenReturn(udsInterceptEntity);

// 		UdsInterceptPublishStatusDTO udsInterceptDto = service
// 				.getLatestUdsInterceptPublishStatusByHandlingUnitUUID("handlingUnitUUID0");

// 		assertThat(udsInterceptDto.getPhuwvEventUUID()).isEqualTo(udsInterceptEntity.getPhuwvEventUUID());

// 	}

// }
