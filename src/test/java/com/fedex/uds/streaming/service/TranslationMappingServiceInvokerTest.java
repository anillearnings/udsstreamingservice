// package com.fedex.uds.streaming.service;

// import static org.assertj.core.api.Assertions.assertThat;

// import java.util.ArrayList;
// import java.util.List;

// import org.junit.Before;
// import org.junit.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.mockito.Mockito;
// import org.mockito.MockitoAnnotations;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.test.context.ActiveProfiles;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// import com.fedex.uds.common.dto.MappingCodeDTO;
// import com.fedex.uds.common.dto.MappingCodesDTO;
// import com.fedex.uds.streaming.client.TranslationMappingServiceClient;
// import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
// import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
// import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;


// @ExtendWith(SpringExtension.class)
// @ActiveProfiles("L0")
// @SpringBootTest
// public class TranslationMappingServiceInvokerTest {

// 	@InjectMocks
//     private TranslationMappingServiceInvoker translationMappingServiceInvoker;

//     @Mock
//     private TranslationMappingServiceClient translationMappingServiceClient;

// 	@Before
//     public void setup() {
//         MockitoAnnotations.initMocks(this);
//     }

// 	@Test
//     public void when_call_mappingCodeDTO_translationMappingServiceInvokeSuccess() throws Exception {

// 		UdsDGMappingEntity entity=new UdsDGMappingEntity();
// 		entity.setOperatingCompanyCd("FX");
// 		entity.setSourceDgTypeCd("04");
// 		UdsServiceCodeMappingEntity serviceEntity=new UdsServiceCodeMappingEntity();
// 		serviceEntity.setServiceCd("25");
// 		serviceEntity.setCommitTm("2359");
// 		UdsHandlingCodeMappingEntity handlingCode=new UdsHandlingCodeMappingEntity();
// 		handlingCode.setHandlingInterceptCd("HS");
// 		MappingCodeDTO dto=new MappingCodeDTO();
// 	    Mockito.when(translationMappingServiceClient.getTranslations(Mockito.any())).thenReturn(ResponseEntity.ok(dto));
// 	    MappingCodeDTO response= translationMappingServiceInvoker.getTranslationsResult(dto);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test(expected = Exception.class)
//     public void when_call_mappingCodeDTO_translationMappingServiceInvokeFailure() throws Exception {
// 		MappingCodeDTO dto=new MappingCodeDTO();
// 	    Mockito.when(translationMappingServiceClient.getTranslations(Mockito.any())).thenReturn(new ResponseEntity<MappingCodeDTO>(HttpStatus.NO_CONTENT));
// 	    MappingCodeDTO response= translationMappingServiceInvoker.getTranslationsResult(dto);
// 	    assertThat(response).isNotNull();
//     }	

// 	@Test
//     public void when_call_udsDGMappingEntities_translationMappingServiceInvokeSuccess() throws Exception {
// 		List<UdsDGMappingEntity> entities=new ArrayList<UdsDGMappingEntity>();
// 		UdsDGMappingEntity entity=new UdsDGMappingEntity();
// 		entity.setOperatingCompanyCd("FX");
// 		entity.setSourceDgTypeCd("04");
// 		entities.add(entity);
// 	    Mockito.when(translationMappingServiceClient.getDgCodes(Mockito.any())).thenReturn(ResponseEntity.ok(entities));
// 	    List<UdsDGMappingEntity> response= translationMappingServiceInvoker.getUdsDGMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test(expected = Exception.class)
//     public void when_call_udsDGMappingEntities_translationMappingServiceInvokeFailure() throws Exception {
// 		List<UdsDGMappingEntity> entities=new ArrayList<UdsDGMappingEntity>();
// 		UdsDGMappingEntity entity=new UdsDGMappingEntity();
// 		entities.add(entity);
// 	    Mockito.when(translationMappingServiceClient.getDgCodes(Mockito.any())).thenReturn(new ResponseEntity<List<UdsDGMappingEntity>>(HttpStatus.NO_CONTENT));
// 	    List<UdsDGMappingEntity> response= translationMappingServiceInvoker.getUdsDGMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test
//     public void when_call_udsHandlingCodeMappingEntities_translationMappingServiceInvokeSuccess() throws Exception {
// 		List<UdsHandlingCodeMappingEntity > entities=new ArrayList<UdsHandlingCodeMappingEntity >();
// 		UdsHandlingCodeMappingEntity handlingCode=new UdsHandlingCodeMappingEntity();
// 		handlingCode.setHandlingInterceptCd("HS");
// 		entities.add(handlingCode);
// 	    Mockito.when(translationMappingServiceClient.getHandlingCodes(Mockito.any())).thenReturn(ResponseEntity.ok(entities));
// 	    List<UdsHandlingCodeMappingEntity> response= translationMappingServiceInvoker. getudsHandlingCodeMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test(expected = Exception.class)
//     public void when_call_udsHandlingCodeMappingEntities_translationMappingServiceInvokeFailure() throws Exception {
// 		List<UdsHandlingCodeMappingEntity > entities=new ArrayList<UdsHandlingCodeMappingEntity >();
// 		UdsHandlingCodeMappingEntity handlingCode=new UdsHandlingCodeMappingEntity();
// 		entities.add(handlingCode);
// 	    Mockito.when(translationMappingServiceClient.getHandlingCodes(Mockito.any())).thenReturn(new ResponseEntity<List<UdsHandlingCodeMappingEntity>>(HttpStatus.NO_CONTENT));
// 	    List<UdsHandlingCodeMappingEntity> response= translationMappingServiceInvoker. getudsHandlingCodeMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test
//     public void when_call_udsServiceCodeMappingEntities_translationMappingServiceInvokeSuccess() throws Exception {
// 		List<UdsServiceCodeMappingEntity> entities=new ArrayList<UdsServiceCodeMappingEntity>();
// 		UdsServiceCodeMappingEntity serviceEntity=new UdsServiceCodeMappingEntity();
// 		serviceEntity.setServiceCd("25");
// 		serviceEntity.setCommitTm("2359");
// 		entities.add(serviceEntity);
// 	    Mockito.when(translationMappingServiceClient.getServiceCodes(Mockito.any())).thenReturn(ResponseEntity.ok(entities));
// 	    List<UdsServiceCodeMappingEntity> response= translationMappingServiceInvoker.getUdsServiceCodeMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test(expected = Exception.class)
//     public void when_call_udsServiceCodeMappingEntities_translationMappingServiceInvokeFailure()throws Exception {
// 		List<UdsServiceCodeMappingEntity> entities=new ArrayList<UdsServiceCodeMappingEntity>();
// 		UdsServiceCodeMappingEntity serviceEntity=new UdsServiceCodeMappingEntity();
// 		entities.add(serviceEntity);
// 	    Mockito.when(translationMappingServiceClient.getServiceCodes(Mockito.any())).thenReturn(new ResponseEntity<List<UdsServiceCodeMappingEntity>>(HttpStatus.NO_CONTENT));
// 	    List<UdsServiceCodeMappingEntity> response= translationMappingServiceInvoker.getUdsServiceCodeMappingEntities(entities);
// 	    assertThat(response).isNotNull();
//     }

// 	@Test
//     public void when_call_mappingCodesDTO_translationMappingServiceInvokeSuccess() throws Exception {

// 		MappingCodesDTO dto=new MappingCodesDTO();
// 	    Mockito.when(translationMappingServiceClient.getAllMappingCodes()).thenReturn(ResponseEntity.ok(dto));
// 	    MappingCodesDTO response= translationMappingServiceInvoker.getAllMappingCodes();
// 	    assertThat(response).isNotNull();
//     }

// 	@Test(expected = Exception.class)
//     public void when_call_mappingCodesDTO_translationMappingServiceInvokeFailure() throws Exception {

// 	    Mockito.when(translationMappingServiceClient.getAllMappingCodes()).thenReturn(new ResponseEntity<MappingCodesDTO>(HttpStatus.NO_CONTENT));
// 	    MappingCodesDTO response= translationMappingServiceInvoker.getAllMappingCodes();
// 	    assertThat(response).isNotNull();
//     }


// }
