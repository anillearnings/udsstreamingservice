
package com.fedex.uds.streaming.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.uds.common.exception.ResourceException;
import com.fedex.uds.util.JsonUtil;

@ExtendWith(SpringExtension.class)

@ActiveProfiles("L0")

@SpringBootTest
class HandlingUnitHandlerTest {

	@Autowired

	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@Mock
	UdsShipmentPublishStatusService udsShipmentPublishStatusService;

	@Mock
	UdsInterceptPublishStatusService udsInterceptPublishStatusService;

	@Mock
	private HandlingUnitHandler handlingUnitMockHandler;

	@Autowired
	private HandlingUnitHandler handlingUnitMockHandle;

	@Test
	public void ideal_test() throws ResourceException, Exception {
		HandlingUnit handlingUnit = JsonUtil.json2Obj(objectMapper, "data", "phuwv_event.json", HandlingUnit.class);

//  Mockito.when(udsShipmentPublishStatusService
//  .getLatestUdsShipmentPublishStatusByHandlingUnitUUID(Mockito.anyString()))
//  .thenReturn(new UdsShipmentPublishStatusDTO());
//  Mockito.when(udsInterceptPublishStatusService
//  .getLatestUdsInterceptPublishStatusByHandlingUnitUUID(Mockito.anyString()))
//  .thenReturn(new UdsInterceptPublishStatusDTO());
//  Mockito.when(phuwvEventsService.
//  getPhuwvEventsEntityByHandlingUnitUUIDAndEventUUID(Mockito.anyString(),
//  Mockito.anyString())).thenReturn(new HandlingUnit()); try { JmsMessageMock
//  message = new JmsMessageMock();
//  message.setJMSCorrelationID("36d83f00-76c8-38c4-a904-c9368f855d6i");
//  SimpleJmsHeaderMapper simpleJmsHeaderMapper = new SimpleJmsHeaderMapper();
//  MessageHeaders messageHeaders = simpleJmsHeaderMapper.toHeaders(message);
//  GenericMessage<HandlingUnit> genericMessage = new
//  GenericMessage<>(handlingUnit, messageHeaders);
//		handlingUnitMockHandle.handle(genericMessage);
//	}
//  }

		handlingUnitMockHandle.handle(new GenericMessage<>(handlingUnit));
	}
}

// //
// //	@Test
// //	public void getLatestUdsShipmentPublishStatusByHandlingUnitUUID_NULL_test() throws ResourceException, Exception {
// //		HandlingUnit handlingUnit = JsonUtil.json2Obj(objectMapper, "data", "phuwv_event.json", HandlingUnit.class);
// //		Mockito.when(udsShipmentPublishStatusService
// //				.getLatestUdsShipmentPublishStatusByHandlingUnitUUID(Mockito.anyString())).thenReturn(null);
// //		handlingUnitMockHandler.handle(new GenericMessage<>(handlingUnit));
// //	}
// //
// //	@Test
// //	public void getPhuwvEventsEntityByHandlingUnitUUIDAndEventUUID_NULL_test() throws ResourceException, Exception {
// //		HandlingUnit handlingUnit = JsonUtil.json2Obj(objectMapper, "data", "phuwv_event.json", HandlingUnit.class);
// //
// //		Mockito.when(udsShipmentPublishStatusService
// //				.getLatestUdsShipmentPublishStatusByHandlingUnitUUID(Mockito.anyString()))
// //				.thenReturn(new UdsShipmentPublishStatusDTO());
// //		Mockito.when(udsInterceptPublishStatusService
// //				.getLatestUdsInterceptPublishStatusByHandlingUnitUUID(Mockito.anyString()))
// //				.thenReturn(new UdsInterceptPublishStatusDTO());
// //		Mockito.when(phuwvEventsService.getPhuwvEventsEntityByHandlingUnitUUIDAndEventUUID(Mockito.anyString(),
// //				Mockito.anyString())).thenReturn(null);
// //		handlingUnitMockHandler.handle(new GenericMessage<>(handlingUnit));
// //	}
// //
// //	@Test
// //	public void getLatestUdsInterceptPublishStatusByHandlingUnitUUID_NULL_test() throws ResourceException, Exception {
// //		HandlingUnit handlingUnit = JsonUtil.json2Obj(objectMapper, "data", "phuwv_event.json", HandlingUnit.class);
// //		Mockito.when(udsInterceptPublishStatusService
// //				.getLatestUdsInterceptPublishStatusByHandlingUnitUUID(Mockito.anyString())).thenReturn(null);
// //		handlingUnitMockHandler.handle(new GenericMessage<>(handlingUnit));
// //	}

// //}
