/*
 * package com.fedex.uds.streaming.service;
 * 
 * import java.io.IOException;
 * 
 * import org.junit.jupiter.api.Test; import
 * org.junit.jupiter.api.extension.ExtendWith; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.beans.factory.annotation.Qualifier; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * org.springframework.messaging.support.GenericMessage; import
 * org.springframework.test.context.ActiveProfiles; import
 * org.springframework.test.context.junit.jupiter.SpringExtension;
 * 
 * import com.fasterxml.jackson.core.JsonParseException; import
 * com.fasterxml.jackson.databind.JsonMappingException; import
 * com.fasterxml.jackson.databind.ObjectMapper; import
 * com.fedex.bre.executorservice.domain.sortmodifier.model.
 * BusinessRuleHandlingCodes; import
 * com.fedex.uds.common.api.models.HandlingUnit; import
 * com.fedex.uds.common.exception.ResourceException;
 * 
 * import com.fedex.uds.util.JsonUtil;
 * 
 * @ExtendWith(SpringExtension.class)
 * 
 * @ActiveProfiles("L0")
 * 
 * @SpringBootTest public class SortModifierServiceHandlerTest {
 * 
 * @Autowired
 * 
 * @Qualifier("defaultObjectMapper") private ObjectMapper objectMapper;
 * 
 * @Autowired // @Qualifier("SortModifierServiceHandler") private
 * SortModifierServiceHandler sortModifierServiceHandler;
 * 
 * @Test public void ideal_test() throws ResourceException, Exception {
 * BusinessRuleHandlingCodes brehandlingUnit = JsonUtil.json2Obj(objectMapper,
 * "data", "SortModifierService.json", BusinessRuleHandlingCodes.class);
 * 
 * sortModifierServiceHandler.handle(new GenericMessage<>(brehandlingUnit));
 * 
 * 
 * } }
 */