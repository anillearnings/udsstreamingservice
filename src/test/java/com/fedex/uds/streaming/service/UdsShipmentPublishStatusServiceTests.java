// package com.fedex.uds.streaming.service;

// import java.io.IOException;
// import java.time.Instant;
// import java.util.Date;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.mockito.InjectMocks;
// import org.mockito.MockitoAnnotations;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.boot.test.mock.mockito.MockBean;

// import com.fasterxml.jackson.core.JsonParseException;
// import com.fasterxml.jackson.databind.JsonMappingException;
// import com.fedex.uds.streaming.dto.UdsShipmentPublishStatusDTO;
// import com.fedex.uds.streaming.repository.UdsShipmentPublishStatusRepository;
// import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
// import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusId;

// import static org.assertj.core.api.Assertions.assertThat;
// import static org.mockito.Mockito.when;

// @SpringBootTest
// public class UdsShipmentPublishStatusServiceTests {

// 	@MockBean
// 	UdsShipmentPublishStatusRepository repository;

// 	UdsShipmentPublishStatusEntity udsShipmentEntity = new UdsShipmentPublishStatusEntity();

// 	@Autowired
// 	@InjectMocks
// 	UdsShipmentPublishStatusService service;

// 	@BeforeEach
// 	public void init() {

// 		MockitoAnnotations.initMocks(this);

// 		UdsShipmentPublishStatusId id = new UdsShipmentPublishStatusId("handlingUnitUUID", Instant.now());

// 		udsShipmentEntity.setUdsShipmentPublishStatusId(id);
// 		udsShipmentEntity.setPhuwvEventUUID("phuwvEventUUID");
// 		udsShipmentEntity.setTntHandlingUnitId("tntHandlingUnitId");
// 		udsShipmentEntity.setEnterpriseHandlingUnitId("enterpriseHandlingUnitId");
// 		udsShipmentEntity.setPublishedCountNbr(1);
// 		udsShipmentEntity.setRowPurgeDt(new Date());

// 	}

// 	@Test
// 	public void test_GetLatestUdsShipmentPublishStatusByHandlingUnitUUID()
// 			throws JsonParseException, JsonMappingException, IOException {

// 		when(repository
// 				.findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(
// 						"handlingUnitUUID0")).thenReturn(udsShipmentEntity);

// 		UdsShipmentPublishStatusDTO udsShipmetDto = service
// 				.getLatestUdsShipmentPublishStatusByHandlingUnitUUID("handlingUnitUUID0");

// 		assertThat(udsShipmetDto.getPhuwvEventUUID()).isEqualTo(udsShipmentEntity.getPhuwvEventUUID());

// 	}

// }
