//  package com.fedex.uds.streaming.service.mapperTest;

//  import static org.junit.Assert.assertEquals;
// import static org.mockito.Mockito.when;

// import java.io.IOException;
// import java.nio.charset.Charset;
// import java.nio.file.Files;
// import java.nio.file.Paths;
// import java.util.ArrayList;
// import java.util.List;

// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.mockito.Mockito;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
// import org.springframework.boot.test.context.SpringBootTest;
// import org.springframework.test.context.ActiveProfiles;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.fedex.phuwv.common.api.models.HandlingUnit;
// import com.fedex.uds.common.dto.MappingCodesDTO;
// import com.fedex.uds.common.dto.idm.MainShipment;
// import com.fedex.uds.common.dto.idm.SHP010;
// import com.fedex.uds.common.dto.idm.SHP170;
// import com.fedex.uds.common.dto.idm.SHP880;
// import com.fedex.uds.streaming.configuration.FormIdCheckProperties;
// import com.fedex.uds.streaming.service.ShipmentIDMConversion;
// import com.fedex.uds.streaming.service.TranslationMappingServiceInvoker;
// import com.fedex.uds.view.dgmappingentity.entity.UdsDGMappingEntity;
// import com.fedex.uds.view.handlingcodemappingentity.entity.UdsHandlingCodeMappingEntity;
// import com.fedex.uds.view.servicecodemappingentity.entity.UdsServiceCodeMappingEntity;

//  @ExtendWith(SpringExtension.class)
//  @ActiveProfiles("L0")
//  @SpringBootTest
//  @AutoConfigureMockMvc
//  public class ShipmentMapperTest {
//  	@InjectMocks
//  	private ShipmentIDMConversion service;
//  	private MainShipment shipment = new MainShipment();
 	
//  	@Mock
// 	private TranslationMappingServiceInvoker mappingServiceInvoker;
 	
//  	@Mock
//  	private FormIdCheckProperties formIdCheckProperties;
 	
 	

//  	@Autowired
// 	private ObjectMapper defaultObjectMapper;
//  	@BeforeEach
//  	public void init() {

//  		SHP010 shipmentHeader = new SHP010();
//  		SHP170 shipmentDetail = new SHP170();
//  		SHP880 shipmentRoute = new SHP880();
//  		ArrayList<SHP880> shipmentRouteList = new ArrayList<SHP880>();
//  		shipmentHeader.setCustomerReferenceNr("794828177766");


//  		for (int i = 0; i <= 2; i++) {
//  			shipmentRoute.setSequencingNr("1"+i);
//  			shipmentRoute.setBul010IdNext("Bul010IdNext");
//  			shipmentRoute.setId("");
//  			shipmentRoute.setBul010IdRouting("");
//  			shipmentRouteList.add(shipmentRoute);


//  		}
//  		shipmentHeader.setPaymentTermsCd("");

//  		shipmentHeader.setId("");
//  		shipmentHeader.setLegacyConNr("");
//  		shipmentHeader.setBul010IdClearance("");
//  		shipmentHeader.setPrd010Id("04");

//  		shipmentDetail.setSourceSystemCd("FX");
//  		shipmentDetail.setContractualVl("0.00");
//  		shipmentDetail.setSpeedofService("");
//  		shipmentDetail.setTotalItemsNr("1");
//  		shipmentDetail.setDomesticShipmentNr("");
//  		shipmentDetail.setDomesticShipmentType("");
//  		shipmentDetail.setRnd010Id("");
//  		shipmentDetail.setLastDepotSortDt("");


//  		shipment.setShipmentRoute(shipmentRouteList);

//  		shipment.setShipmentDetail(shipmentDetail);
//  		shipment.setShipmentHeader(shipmentHeader);
//  	}
 	
// 	public MappingCodesDTO translationMapping() {

// 		MappingCodesDTO mappingCodesDTOs= new MappingCodesDTO();
// 		List<UdsDGMappingEntity> udsDGMappingEntityList = new ArrayList<UdsDGMappingEntity>();
// 		UdsDGMappingEntity entity = new UdsDGMappingEntity();

// 		entity.setSourceDgTypeCd("06");
// 		udsDGMappingEntityList.add(entity);
// 		List<UdsServiceCodeMappingEntity> udsServiceCodeMappingEntityList = new ArrayList<UdsServiceCodeMappingEntity>();
// 		UdsServiceCodeMappingEntity serviceEntity = new UdsServiceCodeMappingEntity();

// 		serviceEntity.setServiceCd("RM"); // (handlingUnit.getLegacyServiceCode()); // hard code here for demo
// 		serviceEntity.setCommitTm("2359");
// 		udsServiceCodeMappingEntityList.add(serviceEntity);

// 		// Code Below Must be removed
// 		List<UdsHandlingCodeMappingEntity> udsHandlingCodeMappingEntityList = new ArrayList<UdsHandlingCodeMappingEntity>();
// 		UdsHandlingCodeMappingEntity udshandlingentity = new UdsHandlingCodeMappingEntity();
// 		udshandlingentity.setHandlingInterceptCd("CUD"); // hard coded and must be removed
// 		udsHandlingCodeMappingEntityList.add(udshandlingentity);

// 		mappingCodesDTOs.setUdsDGMappingEntities(udsDGMappingEntityList);
// 		mappingCodesDTOs.setUdsHandlingCodeMappingEntities(udsHandlingCodeMappingEntityList);
// 		mappingCodesDTOs.setUdsServiceCodeMappingEntities(udsServiceCodeMappingEntityList);
// 		return mappingCodesDTOs;
// 	}

//  	@Test
//  	public void testperforShipmentIdmConversion() throws Exception {
//  		String contents;
// 		HandlingUnit handlingUnitData=null;
// 		try {
// 			contents = readFile("src/test/resources/data/phuwv_event.json", Charset.defaultCharset());
// 			handlingUnitData = defaultObjectMapper.readValue(contents, HandlingUnit.class);
// 		} catch (IOException e) {
// 			e.printStackTrace();
// 		}
// 		MappingCodesDTO translationMapping = translationMapping();
// 		List<String> list=new ArrayList<>();
// 		list.add("1101");
		
// when(formIdCheckProperties.getFormId()).thenReturn(list);
// 		when(mappingServiceInvoker.getTranslationsResult(Mockito.any(MappingCodesDTO.class)))
// 				.thenReturn(translationMapping);

// 		MainShipment perforShipmentIdmConversion = service.perforShipmentIdmConversion( handlingUnitData);


//  		assertEquals(null,perforShipmentIdmConversion.getShipmentHeader().getCustomerReferenceNr());


//  	}
 	
//  	@Test
//  	public void testperforShipmentIdmConversion1() throws Exception {
//  		String content;
// 		HandlingUnit handlingUnit = null;
// 		try {
// 			content = readFile("src/test/resources/data/phuwv_event2.json", Charset.defaultCharset());
// 			handlingUnit = defaultObjectMapper.readValue(content, HandlingUnit.class);
// 		} catch (IOException e) {
// 			e.printStackTrace();
// 		}
// 		MappingCodesDTO translationMapping = translationMapping();
// 		List<String> list=new ArrayList<>();
// 		list.add("1101");
		
// when(formIdCheckProperties.getFormId()).thenReturn(list);
// 		when(mappingServiceInvoker.getTranslationsResult(Mockito.any(MappingCodesDTO.class)))
// 				.thenReturn(translationMapping);
// 		MainShipment perforShipmentIdmConversion = service.perforShipmentIdmConversion( handlingUnit);


//  		assertEquals("794828177766",perforShipmentIdmConversion.getShipmentHeader().getCustomerReferenceNr());


//  	}


// 	static String readFile(String path, Charset encoding) throws IOException {
// 		byte[] encoded = Files.readAllBytes(Paths.get(path));
// 		return new String(encoded, encoding);
// 	}
//  }
