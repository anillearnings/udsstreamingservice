package com.fedex.uds.streaming.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.util.CompressionUtil;
import com.fedex.uds.util.JsonUtil;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusEntity;
import com.fedex.uds.view.interceptpublishentity.entity.UdsInterceptPublishStatusId;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusEntity;
import com.fedex.uds.view.shipmentpublishentity.entity.UdsShipmentPublishStatusId;
import com.sun.el.parser.ParseException;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@ActiveProfiles("L0")
public class UdsShipmentPublishStatusRepositoryTests {

	@MockBean
	UdsShipmentPublishStatusRepository repository;

	@Autowired
	private ObjectMapper objectMapper;

	private int maxDataImgColumnLength = 2000;
	private int maxDataImgColumn = 2;
	private int maxDataRetentionDays = 30;
	private HandlingUnit coreHandlingUnit;
	UdsShipmentPublishStatusEntity shipmentEntity = null;
	private static final String HU_UUID = "042b4968-4df6-374e-b109-3fb79cd50863";
	private static final String PHUWV_EVENT_UUID = "042b4968-4df6-374e-b109-3fb79cd50863";
	private static final String UDS_INTERCEPT_CD = "COD";
	private static final String UDS_PRIMARY_INTERCEPT_CD = "CGNR";
	private static final String TRACKING_NUMBER = "794828305230";
	List<UdsShipmentPublishStatusEntity> list = new ArrayList<UdsShipmentPublishStatusEntity>();
	List<String> listString = new ArrayList<String>();

	@BeforeEach
	public void setup() throws ParseException, IOException {
		// Pull CORE Trip from JSON file
		String content = readFile("src/test/resources/data/phuwv_event.json", Charset.defaultCharset());
		coreHandlingUnit = objectMapper.readValue(content, HandlingUnit.class);

		// Build SortModifierServiceEntity with BusinessRuleHandlingCodes
		shipmentEntity = new UdsShipmentPublishStatusEntity();

		UdsShipmentPublishStatusId shipmentId = new UdsShipmentPublishStatusId(coreHandlingUnit.getHuUUID(),
				Instant.now());
		shipmentEntity.setUdsShipmentPublishStatusId(shipmentId);
		shipmentEntity.setRowPurgeDt(getDefaultPurgeDate());

		list.add(shipmentEntity);
		String json = JsonUtil.asCompactJson(objectMapper, coreHandlingUnit);
		convertJsonToRaw(json, shipmentEntity);
		// Mock saving TaskEntity to database
		Mockito.when(repository.findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(TRACKING_NUMBER))
				.thenReturn(list);
		Mockito.when(repository.findByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(list);
		Mockito.when(repository.findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(shipmentEntity);
		Mockito.when(repository.findTopByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(shipmentEntity);
	}

	@Test
	void testSavingTaskMockReturn() throws Exception {
		Mockito.when(repository.findFirstByEnterpriseHandlingUnitIdOrderByUdsrowUpdateTmstpDesc(TRACKING_NUMBER))
		.thenReturn(list);
Mockito.when(repository.findByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(list);
Mockito.when(repository.findFirstByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(shipmentEntity);
Mockito.when(repository.findTopByUdsShipmentPublishStatusId_handlingUnitUUIDOrderByUdsShipmentPublishStatusId_rowUpdateTmstpDesc(HU_UUID)).thenReturn(shipmentEntity);
		assertThat(list).isNotNull();
	}

	private void convertJsonToRaw(String compactJson, UdsShipmentPublishStatusEntity entity) throws IOException {
		int maxColumnLength = this.maxDataImgColumnLength;
		int totalColumnLength = maxDataImgColumn * maxColumnLength;
		byte[] byteBuff = CompressionUtil.gzip(compactJson);

		List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
		int dataImageToSet = dataImages.size();
		entity.setShipmentDataImg1(dataImages.get(0));
		if (dataImageToSet > 1) {
			entity.setShipmentDataImg2(dataImages.get(1));
		}
	}

	private Date getDefaultPurgeDate() {
		Instant now = Instant.now();
		Instant purgeInstant = now.plus(maxDataRetentionDays, ChronoUnit.DAYS);
		Date date = Date.from(purgeInstant);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

}