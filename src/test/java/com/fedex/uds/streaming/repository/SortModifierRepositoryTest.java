package com.fedex.uds.streaming.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.bre.executorservice.domain.sortmodifier.model.BusinessRuleHandlingCodes;
import com.fedex.uds.util.BytesUtil;
import com.fedex.uds.util.CompressionUtil;
import com.fedex.uds.util.JsonUtil;
import com.fedex.uds.view.sortmodifierentity.entity.SortModifierServiceEntity;
import com.fedex.uds.view.sortmodifierentity.entity.SortModifierServiceId;
import com.sun.el.parser.ParseException;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@ActiveProfiles("L0")
public class SortModifierRepositoryTest {
	
	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private SortModifierRepository repository;

	private int maxDataImgColumnLength = 2000;
	private int maxDataImgColumn = 2;
	private int maxDataRetentionDays = 30;
	private BusinessRuleHandlingCodes coreBre;
	SortModifierServiceEntity breEntity = null;
	private static final String HU_UUID = "8e0e1b9b-c1bf-3cda-a1ba-30a65c2f339a";
	
	@BeforeEach
	public void setup() throws ParseException, IOException {
		// Pull CORE Trip from JSON file
		String content = readFile("src/test/resources/data/SortModifierService.json", Charset.defaultCharset());
		coreBre = objectMapper.readValue(content, BusinessRuleHandlingCodes.class);

		// Build SortModifierServiceEntity with BusinessRuleHandlingCodes
		breEntity = new SortModifierServiceEntity();
		SortModifierServiceId breId= new SortModifierServiceId(coreBre.getHuUUID(),Instant.now());
		breEntity.setSortModifierServiceId(breId);
		breEntity.setEventCreateTimestamp(ZonedDateTime.now());
		breEntity.setRowPurgeDt(getDefaultPurgeDate());
		String json = JsonUtil.asCompactJson(objectMapper, coreBre);
		convertJsonToRaw(json, breEntity);
		//Mock saving TaskEntity to database
		Mockito.when(repository.findFirstBySortModifierServiceId_handlingUnitUUIDOrderByEventCreateTimestampDesc(HU_UUID))
				.thenReturn(breEntity);
	}
	
	@Test
	 void testSavingTaskMockReturn() throws Exception {
		Mockito.when(repository.findFirstBySortModifierServiceId_handlingUnitUUIDOrderByEventCreateTimestampDesc(coreBre.getHuUUID())).thenReturn(breEntity);
		assertThat(breEntity).isNotNull();
	}
	
	private void convertJsonToRaw(String compactJson, SortModifierServiceEntity entity) throws IOException {
		int maxColumnLength = this.maxDataImgColumnLength;
		int totalColumnLength = maxDataImgColumn * maxColumnLength;
		byte[] byteBuff = CompressionUtil.gzip(compactJson);

		List<byte[]> dataImages = BytesUtil.convertJsonByteArray(byteBuff, maxColumnLength, totalColumnLength);
		int dataImageToSet = dataImages.size();
		entity.setDataImg1(dataImages.get(0));
		if (dataImageToSet > 1) {
			entity.setDataImg2(dataImages.get(1));
		}
	}

	
	private Date getDefaultPurgeDate() {
		Instant now = Instant.now();
		Instant purgeInstant = now.plus(maxDataRetentionDays, ChronoUnit.DAYS);
		Date date = Date.from(purgeInstant);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}


}
